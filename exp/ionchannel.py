from tqdm import tqdm
import numpy as np
import tables
import src.model.VarDist as VarDist

EPS = 1e-10

if __name__ == '__main__':
    np.random.seed(123)

    IN_PATH = '../data/ionchannel/input/'
    OUT_PATH = '../data/ionchannel/output/'

    L = 10  # aka infinity, no. of modes/states

    data = np.genfromtxt(IN_PATH + f'+160mV_1min.asc')
    data = data[None, None, 90000:240000]
    N = 1
    d = 1
    T = data.shape[2]

    n_runs = 10
    n_iter = 1000
    n_modes = np.zeros((n_runs, n_iter + 1))
    epsrel = 1e-5
    save = True

    import time
    t = time.localtime()
    timestamp = time.strftime("%d-%m-%Y_%H-%M-%S", t)

    if save:
        h5file = tables.open_file(OUT_PATH + f"{timestamp}_results.h5", 'a')
        h5file.create_group(h5file.root, 'q_z')
        h5file.create_group(h5file.root, 'concentration_transition')
        h5file.create_group(h5file.root, 'concentration_initial')
        h5file.create_group(h5file.root, 'niw_mu0')
        h5file.create_group(h5file.root, 'iw_dof')
        h5file.create_group(h5file.root, 'iw_scale')
        h5file.create_group(h5file.root, 'niw_lambda')
        h5file.create_group(h5file.root, 'beta')
        h5file.create_group(h5file.root, 'ELBO')
        h5file.create_group(h5file.root, 'hpar')
        h5file.close()

    for run in tqdm(range(n_runs)):
        start = time.time()
        iw_dof = 0.01 * N * T * np.ones(L)
        tmp = np.einsum('ndt->ntd', data)
        iw_scale = np.cov(tmp.reshape((-1, d)).T) * (iw_dof - d - 1)[:, None, None]
        niw_mu0 = np.tile(np.mean(data, axis=(0, 2)), (L, 1))
        niw_lambda = 0.01 * N * T * np.ones(L)
        top_level_gem_scaling = 0.6
        subordinate_gem_scaling = 0.6
        stickyness = 0.01 * N * T

        q = VarDist(data, truncation_level=L, iw_dof=iw_dof, iw_scale=iw_scale, niw_mu0=niw_mu0,
                    niw_lambda=niw_lambda, top_level_gem_scaling=top_level_gem_scaling,
                    subordinate_gem_scaling=subordinate_gem_scaling, stickyness=stickyness)

        elbo_old = np.NINF

        if save:
            # Create groups for this run
            h5file = tables.open_file(OUT_PATH + f"{timestamp}_results.h5", 'a')
            h5file.create_group(h5file.root.q_z, f"run_{run}")
            h5file.create_group(h5file.root.concentration_transition, f"run_{run}")
            h5file.create_group(h5file.root.concentration_initial, f"run_{run}")
            h5file.create_group(h5file.root.niw_mu0, f"run_{run}")
            h5file.create_group(h5file.root.iw_scale, f"run_{run}")
            h5file.create_group(h5file.root.iw_dof, f"run_{run}")
            h5file.create_group(h5file.root.niw_lambda, f"run_{run}")
            h5file.create_group(h5file.root.beta, f"run_{run}")
            h5file.create_group(h5file.root.hpar, f"run_{run}")
            # Save hyperparameters (as these are not changed over runs)
            h5file.create_array(h5file.root.hpar[f"run_{run}"], "stickyness", q.par.stickyness)
            h5file.create_array(h5file.root.hpar[f"run_{run}"], "top_level_gem_scaling", q.hpar.top_level_gem_scaling)
            h5file.create_array(h5file.root.hpar[f"run_{run}"], "subordinate_gem_scaling", q.hpar.subordinate_gem_scaling)
            h5file.create_array(h5file.root.hpar[f"run_{run}"], "iw_dof", q.hpar.iw_dof)
            h5file.create_array(h5file.root.hpar[f"run_{run}"], "iw_scale", q.hpar.iw_scale)
            h5file.create_array(h5file.root.hpar[f"run_{run}"], "niw_lambda", q.hpar.niw_lambda)
            h5file.create_array(h5file.root.hpar[f"run_{run}"], "niw_mu0", q.hpar.niw_mu0)
            h5file.close()

        # Evaluate number of modes covering 99% of the data
        coverage_per_mode = np.mean(q.q_z, axis=(0, 2))
        coverage_per_mode_decreasing = np.sort(coverage_per_mode)[::-1]
        csum = np.cumsum(coverage_per_mode_decreasing)
        relevant_modes = np.where(csum >= .99)[0][0] + 1
        n_modes[run, 0] = relevant_modes

        for it in range(n_iter):
            q.step()

            elbo = q.ELBO[-1]

            # Evaluate number of modes covering 99% of the data
            coverage_per_mode = np.mean(q.q_z, axis=(0, 2))
            coverage_per_mode_decreasing = np.sort(coverage_per_mode)[::-1]
            csum = np.cumsum(coverage_per_mode_decreasing)
            relevant_modes = np.where(csum >= .99)[0][0] + 1
            n_modes[run, it + 1] = relevant_modes

            if save:
                # Save data
                if it in [0, 999, n_iter - 1]:
                    h5file = tables.open_file(OUT_PATH + f"{timestamp}_results.h5", 'a')
                    h5file.create_array(h5file.root.q_z[f'run_{run}'], f"iteration_{it}", q.q_z)
                    h5file.create_array(h5file.root.concentration_transition[f'run_{run}'], f"iteration_{it}",
                                        q.par.concentration_transition)
                    h5file.create_array(h5file.root.concentration_initial[f'run_{run}'], f"iteration_{it}",
                                        q.par.concentration_initial)
                    h5file.create_array(h5file.root.niw_mu0[f'run_{run}'], f"iteration_{it}",
                                        q.par.niw_mu0)
                    h5file.create_array(h5file.root.iw_scale[f'run_{run}'], f"iteration_{it}",
                                        q.par.iw_scale)
                    h5file.create_array(h5file.root.iw_dof[f'run_{run}'], f"iteration_{it}",
                                        q.par.iw_dof)
                    h5file.create_array(h5file.root.niw_lambda[f'run_{run}'], f"iteration_{it}",
                                        q.par.niw_lambda)
                    h5file.create_array(h5file.root.beta[f'run_{run}'], f"iteration_{it}", q.par.beta)

            # Check for convergence
            if np.abs(elbo - elbo_old) / np.abs(elbo_old) < epsrel:
                if save:
                    h5file = tables.open_file(OUT_PATH + f"{timestamp}_results.h5", 'a')
                    h5file.create_array(h5file.root.q_z[f'run_{run}'], f"iteration_{it}", q.q_z)
                    h5file.create_array(h5file.root.concentration_transition[f'run_{run}'], f"iteration_{it}",
                                        q.par.concentration_transition)
                    h5file.create_array(h5file.root.concentration_initial[f'run_{run}'], f"iteration_{it}",
                                        q.par.concentration_initial)
                    h5file.create_array(h5file.root.niw_mu0[f'run_{run}'], f"iteration_{it}",
                                        q.par.niw_mu0)
                    h5file.create_array(h5file.root.iw_scale[f'run_{run}'], f"iteration_{it}",
                                        q.par.iw_scale)
                    h5file.create_array(h5file.root.iw_dof[f'run_{run}'], f"iteration_{it}",
                                        q.par.iw_dof)
                    h5file.create_array(h5file.root.niw_lambda[f'run_{run}'], f"iteration_{it}",
                                        q.par.niw_lambda)
                    h5file.create_array(h5file.root.ELBO, f'run_{run}', q.ELBO)
                    h5file.create_array(h5file.root.beta[f'run_{run}'], f"iteration_{it}", q.par.beta)
                    h5file.close()
                print(q.ELBO[-1], relevant_modes)
                end = time.time()
                print((end - start)/60)
                break

            elbo_old = elbo

    if save:
        h5file = tables.open_file(OUT_PATH + f"{timestamp}_results.h5", 'a')
        h5file.create_array(h5file.root, "modes_vs_iterations", n_modes)
        h5file.close()
