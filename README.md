# Nonparametric Bayesian inference for meta-stable conformational dynamics

This is the companion code and experimental data to the paper "Nonparametric Bayesian inference for meta-stable conformational dynamics".

The experimental data on switching ion channels is stored in `/data/ionchannel/input/`;
The MD simulation data on alanine dipeptide is stored in `/data/alanine_dipeptide/input`.

To reproduce the figures in the paper,

1. generate synthetic data by running the respective `gen_*` files in `/src`:
   1. `gen_3state_gauss_hmm`: data for figure 2
   2. `gen_SDE_3well_potential`: data for figure 3
   3. `gen_angular_data`: data for figure 4
2. run the inference method in on this data via the scripts in `/exp`:
   1. `3state_synNIW`: inference for figure 2
   2. `3well_SDE`: inference for figure 3
   3. `3state_synBvM`: inference for figure 4
   4. `AlaDipeptide_wrapped_NIW`: inference for figure 5
   5. `ionchannel`: inference for figure 6
3. plot the results via methods in `/eval`:
   1. `plot_NIW`: generates figure 2
   2. `plot_NIW_SDE`: generates figure 3
   3. `plot_wrapped_NIW`: generates figure 4
   4. `plot_wrapped_NIW_Ala`: generates figure 5
   5. `plot_NIW_ionchannel`: generates figure 6
