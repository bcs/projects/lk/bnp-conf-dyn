import numpy as np
np.random.seed(1)

N = 10
T = 1000
D = 2

A_pos = np.array([[0.99, 0.01, 0],
                  [0, 0.99, 0.01],
                  [0.01, 0, 0.99]])
pi_pos = np.array([1/3, 1/3, 1/3])

mu = np.zeros((3, 2))
mu[0] = np.array([-3, -3])
mu[1] = np.array([3, -3])
mu[2] = np.array([0, 2])
sig = np.zeros((3, 2, 2))
sig[0] = np.array([[2, 1],
                   [1, 2]])
sig[1] = np.array([[2, -1],
                   [-1, 2]])
sig[2] = np.eye(2) * np.array([5, 2])

z_pos = np.zeros((N, T))
x_pos = np.zeros((N, D, T))
for n in range(N):
    z_pos[n, 0] = np.random.choice([0, 1, 2], p=pi_pos)
    x_pos[n, :, 0] = np.random.multivariate_normal(mu[int(z_pos[n, 0])], sig[int(z_pos[n, 0])])

    for t in range(1, T):
        z_pos[n, t] = np.random.choice([0, 1, 2], p=A_pos[int(z_pos[n, t-1])])
        x_pos[n, :, t] = np.random.multivariate_normal(mu[int(z_pos[n, t])], sig[int(z_pos[n, t])])


np.save(f'../data/3state_N_hmm/input/x_pos_{N}_{D}_{T}', x_pos)
np.save(f'../data/3state_N_hmm/input/z_pos_{N}_{D}_{T}', z_pos)
