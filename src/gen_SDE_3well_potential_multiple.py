"""
Generate synthetic data from 3-well-potential according to Wu et al., JCP, 2017.
"""
from mpl_toolkits.mplot3d import Axes3D  # for 2D-surface of potential
from matplotlib import cm
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()


def U(x, y):
    """
    Potential as defined in Wu et al.

    Parameter
    ---------
    x, y : float
        2D-position
    """
    return (3 * np.exp(-x**2 - (y - 1/3)**2) - 3 * np.exp(-x**2 - (y - 5/3)**2) - 5 * np.exp(-(x-1)**2 - y**2)
            - 5 * np.exp(-(x+1)**2 - y**2) + 1/5 * x**4 + 1/5 * (y - 1/3)**4)


def grad_U(x, y):
    """
    Gradient of potential.
    """
    dx = (3 * np.exp(-x**2 - (y - 1/3)**2) * -2 * x - 3 * np.exp(-x**2 - (y - 5/3)**2) * -2 * x
          -5 * np.exp(-(x-1)**2 - y**2) * -2 * (x-1) - 5 * np.exp(-(x+1)**2 - y**2) * -2 * (x+1)
          + 4/5 * x**3)
    dy = (3 * np.exp(-x**2 - (y - 1/3)**2) * -2 * (y - 1/3) - 3 * np.exp(-x**2 - (y - 5/3)**2) * -2 * (y - 5/3)
          - 5 * np.exp(-(x-1)**2 - y**2) * -2 * y - 5 * np.exp(-(x+1)**2 - y**2) * -2 * y
          + 4/5 * (y - 1/3)**3)
    res = np.array([dx, dy])
    return res

N_datasets = 10
N = 3  # number of trajectories
T = 10000  # number of time points per trajectory

beta = 1.6
dt = 0.05
x = np.zeros((N_datasets, N, 2, T))

for nd in range(N_datasets):
    for n in range(N):
        x[nd, n, 0, 0] = np.random.uniform(-2, -1.5)
        x[nd, n, 1, 0] = np.random.uniform(-1.5, 2.5)

        for t in range(1, T):
            dx = -1 * grad_U(x[nd, n, 0, t - 1], x[nd, n, 1, t - 1]) * dt + np.sqrt(2/beta) * np.random.normal(loc=0, scale=np.sqrt(dt), size=2)
            x[nd, n, :, t] = x[nd, n, :, t - 1] + dx

np.save(f'../data/3well_SDE/input/x_{N}_{T}_multiple', x)

