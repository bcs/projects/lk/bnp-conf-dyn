"""
This file implements Variational Inference for the HDP-HMM with multivariate Gaussian emissions
via the direct assignment truncation, c.f. Matt Johnson's thesis.
"""
from bunch import Bunch
import copy
import numpy as np
import scipy.stats as stats
import scipy.special as special
from src.utils import JIT

EPS = 1e-10


class VarDist:

    def __init__(self, data: np.ndarray, truncation_level: int, iw_dof: float, iw_scale: np.ndarray,
                 niw_mu0: np.ndarray, niw_lambda: float, top_level_gem_scaling: float,
                 subordinate_gem_scaling: float, stickyness: float, periodic: bool = False):
        """

        :param data:
        :param truncation_level:
        :param iw_dof:
        :param iw_scale:
        :param niw_mu0:
        :param niw_lambda:
        :param top_level_gem_scaling:
        :param subordinate_gem_scaling:
        :param stickyness:
        """
        # General model parameters
        self.data = data
        self.truncation_level = truncation_level  # aka. ``L''
        self.n_seq = data.shape[0]  # no. of sequences
        self.n_dim = data.shape[1]  # no. of dimensions (x_t \in \R^n_dim)
        self.n_timepoints = data.shape[2]  # no. of time points per sequence
        self.periodic = periodic

        # Hyperparameters
        self.hpar = Bunch()

        # GEM-hyperparameters
        self.hpar.top_level_gem_scaling = top_level_gem_scaling  # pertains to base measure H1
        self.hpar.subordinate_gem_scaling = subordinate_gem_scaling  # pertains to derived measures (transition rows) pi

        # NIW-hyperparameters
        self.hpar.iw_dof = iw_dof
        self.hpar.niw_lambda = niw_lambda
        self.hpar.niw_mu0 = niw_mu0
        self.hpar.iw_scale = iw_scale
        self.hpar.det_iw_scale = np.linalg.det(self.hpar.iw_scale)

        # Distribution parameters
        self.par = Bunch()
        self.par.top_level_gem_scaling = self.hpar.top_level_gem_scaling
        self.par.subordinate_gem_scaling = self.hpar.subordinate_gem_scaling
        self.par.beta = self.compute_GEM()

        self.par.concentration_transition = np.tile(self.par.subordinate_gem_scaling * self.par.beta, (self.truncation_level, 1))  # concentration parameters of transition Dir distribution
        self.par.concentration_initial = self.par.subordinate_gem_scaling * self.par.beta  # concentration parameters of initial Dir distribution

        self.par.stickyness = stickyness
        self.par.concentration_transition[:, :-1] += self.par.stickyness * np.eye(self.truncation_level)

        # NIW-parameters
        self.par.iw_dof = self.hpar.iw_dof
        self.par.niw_lambda = self.hpar.niw_lambda
        self.par.iw_scale = self.hpar.iw_scale

        # Hack because I dont know how to draw .rvs() with a fixed dimensionaltiy
        # TODO change the 1e-3 into something adaptive
        if self.n_dim == 1:
            tmp_cov = np.cov(np.concatenate(self.data, axis=1))
            tmp_cov = tmp_cov[None, None]
            tmp = stats.multivariate_normal(mean=np.zeros(self.n_dim), cov=1e-3 * tmp_cov).rvs(self.truncation_level)
            tmp = tmp[..., None]
        else:
            tmp_cov = np.cov(np.concatenate(self.data, axis=1))
            tmp = stats.multivariate_normal(mean=np.zeros(self.n_dim), cov=1e-3 * tmp_cov).rvs(size=(self.truncation_level, 1))

        self.par.niw_mu0 = self.hpar.niw_mu0 + tmp
        self.iw_scale_inv = np.linalg.inv(self.par.iw_scale)
        self.det_iw_scale_inv = np.linalg.det(self.iw_scale_inv)

        # Wrap data
        if self.periodic:
            self.data_wrapped = self.wrap_data()

        # Forward- and backward-messages
        self.ln_alpha = np.zeros((self.n_seq, self.truncation_level, self.n_timepoints))
        self.ln_beta = np.zeros((self.n_seq, self.truncation_level, self.n_timepoints))
        self.ln_zeta = np.zeros((self.n_seq, self.n_timepoints))

        self.ln_G_initial = self.digamma_sum(self.par.concentration_initial)
        self.ln_G = self.digamma_sum(self.par.concentration_transition)

        # Initialise distributions and required expectations
        self.log_q_z, self.log_q_zz = self.initialize_q_latent()
        self.q_z = np.exp(self.log_q_z)
        self.q_zz = np.exp(self.log_q_zz)
        self.loglike_emission = self.evaluate_log_emission_likelihood_NIW()

        self.ELBO = []
        self.loglike_GEM = []
        self.neg_KL_NIW = []
        self.neg_KL_Dir = []
        self.neg_KL_Dir_init = []
        self.log_Z = []

    @staticmethod
    def digamma_sum(concentration):
        """
        Return E[ln X_i] = \psi(\alpha_i) - \psi(\sum_j \alpha_j), where X is Dir-distributed.
        """
        return special.digamma(concentration) - special.digamma(np.sum(concentration, axis=-1, keepdims=True))

    def compute_GEM(self):
        """
        Compute one [truncation_level+1,] - dimensional stick-breaking measure.
        :return:
        """
        v = stats.beta(1, self.hpar.top_level_gem_scaling).rvs(self.truncation_level)

        s = np.zeros(self.truncation_level + 1)  # stick portions
        s[0] = v[0]
        for l in range(1, self.truncation_level):
            s[l] = v[l] * np.prod((1 - v[:l]))

        s[-1] = 1 - np.sum(s[:-1])

        # ensure > 0 -ness of beta sticks to avoid numerical issues
        s[s < EPS] = EPS
        s /= np.sum(s)

        return s

    def wrap_data(self):
        """
        Wrap the data and project them to a [-pi, pi] interval around the mode means.

        :return: [N, d, T] array
        """
        distances = self.data[:, None, :, :] - self.par.niw_mu0[None, :, :, None]
        wrapped = self.data[:, None, :, :] - (np.abs(distances) > np.pi) * np.sign(distances) * 2 * np.pi * (1 + np.floor(np.abs((distances - np.sign(distances) * np.pi)/(2*np.pi))))
        return wrapped

    def initialize_q_latent(self):
        """
        Return the one- and two-point-marginals resulting from the given initial distributions.

        Returns:
            log_q_z : array, (n_seq, truncation_level, n_timepoints)
                Holds the logs of the marginals q(z_i), entry (n, l, t) giving ln( q(z^n_t = l) )
            log_q_zz : array, (n_seq, truncation_level, truncation_level, n_timepoints-1)
                Holds the logs of the corresponding two-point marginals.

        """
        log_q_z = np.zeros((self.n_seq, self.truncation_level, self.n_timepoints))  # marginals q(z_i)
        log_q_zz = np.zeros((self.n_seq, self.truncation_level, self.truncation_level, self.n_timepoints - 1))  # two-point marginals q(z_i-1, z_i)

        q_z_init = np.random.uniform(size=(self.n_seq, self.truncation_level))
        q_z_init /= np.sum(q_z_init, axis=1)[:, None]
        log_q_z_init = np.log(q_z_init)

        q_z_trans = np.random.uniform(size=(self.n_seq, self.truncation_level, self.truncation_level, self.n_timepoints - 1))
        q_z_trans /= np.sum(q_z_trans, axis=2)[:, :, None, :]
        log_q_z_cond_init = np.log(q_z_trans)

        log_q_z[:, :, 0] = log_q_z_init
        for t in range(self.n_timepoints - 1):
            log_q_zz[:, :, :, t] = log_q_z[:, :, None, t] + log_q_z_cond_init[:, :, :, t]
            log_q_z[:, :, t + 1] = special.logsumexp(log_q_zz[:, :, :, t], axis=1)

        return log_q_z, log_q_zz

    def update_NIW(self):
        """
        Update the parameters of the NIW emission distribution (niw_mu0, niw_lambda, iw_scale, iw_dof).

        Sets
        ----
        self.par.mu0, .niw_lambda, .iw_scale, .iw_dof

        Returns
        -------
        None
        """
        # Compute the sum of the mode-specific marginals
        Q = np.sum(self.q_z, axis=(0, 2))

        # Compute mode-specific data means
        if self.periodic:
             x_weighted = self.data_wrapped * self.q_z[:, :, None, :]
        else:
            x_weighted = self.data[:, None, :, :] * self.q_z[:, :, None, :]  # CHECKED
        x_bar = np.mean(x_weighted, axis=(0, 3))  # CHECKED

        # Compute posterior mean vectors mu_tilde
        den = self.hpar.niw_lambda + Q  # CHECKED
        num = self.hpar.niw_lambda[:, None] * self.hpar.niw_mu0 + self.n_seq * self.n_timepoints * x_bar  # CHECKED
        mu_tilde = num/den[:, None]  # CHECKED
        if self.periodic:
            mu_tilde[mu_tilde > 2*np.pi] -= 2 * np.pi
            mu_tilde[mu_tilde < 0] += 2 * np.pi
        self.par.niw_mu0 = mu_tilde  # CHECKED

        # Compute posterior psi_tilde
        V = (self.hpar.niw_lambda * (Q - self.n_seq * self.n_timepoints) / den)[:, None, None] * np.einsum('ld,le->lde', self.hpar.niw_mu0, self.hpar.niw_mu0)
        M = ((self.hpar.niw_lambda * self.n_timepoints * self.n_seq) / (self.hpar.niw_lambda + Q))[:, None, None] * np.einsum('ld,le->lde', x_bar - self.hpar.niw_mu0, x_bar - self.hpar.niw_mu0)

        if self.periodic:
            xxT = np.einsum('nldt,nlet->nldet', self.data_wrapped, self.data_wrapped)
            xxT_weighted = np.sum(np.einsum('nldet,nlt->nldet', xxT, self.q_z), axis=(0, 4))
        else:
            xxT = np.einsum('ndt,net->ndet', self.data, self.data)
            xxT_weighted = np.sum(np.einsum('ndet,nlt->nldet', xxT, self.q_z), axis=(0, 4))
        xbarxbarT = ((self.hpar.niw_lambda + self.n_seq * self.n_timepoints) / (self.hpar.niw_lambda + Q))[:, None, None] * np.einsum('ld,le->lde', x_bar, x_bar)
        S = xxT_weighted - self.n_seq * self.n_timepoints * xbarxbarT

        # Compute and update posterior scale/dof-parameters
        self.par.iw_dof = self.hpar.iw_dof + Q  # CHECKED
        self.par.niw_lambda = self.hpar.niw_lambda + Q  # CHECKED
        self.par.iw_scale = self.hpar.iw_scale + M + V + S

        self.data_wrapped = self.wrap_data()

        # Update derived quantities
        self.iw_scale_inv = np.linalg.inv(self.par.iw_scale)
        self.det_iw_scale_inv = np.linalg.det(self.iw_scale_inv)
        self.loglike_emission = self.evaluate_log_emission_likelihood_NIW()

    def digamma_p(self):
        summand = np.array([special.digamma((self.par.iw_dof + 1 - i)/2) for i in range(1, self.n_dim + 1)])
        return np.sum(summand, axis=0)

    def E_ln_det_sig(self):
        """
        Return the value of the expected log determinant of all truncation_level different Sigmas.

        Returns:
            array, (truncation_level, n_dim, n_dim)
        """
        digamma_p = self.digamma_p()
        return digamma_p + self.n_dim * np.log(2) + np.log(self.det_iw_scale_inv)

    def E_quadratic(self):
        """
        Return the value of the expected value of the quadratic form.

        Returns
        -------
        array, (n_seq, truncation_level, n_timepoints)
        """
        # calculate the expectation of a Wishart-distributed r.v., \E_\Sigma[\Sigma^{-1}]
        A = self.par.iw_dof[:, None, None] * self.iw_scale_inv

        # compute mode-constant contribution
        A_tmp = np.einsum('ijk,ik->ij', A, self.par.niw_mu0)
        mode_const = self.n_dim / self.par.niw_lambda + np.einsum('ij,ij->i', self.par.niw_mu0, A_tmp)

        # compute mixed (data-mode) contributions
        if self.periodic:
            Ax = np.einsum('led,nldt->nlte', A, self.data_wrapped)
        else:
            Ax = np.einsum('led,ndt->nlte', A, self.data)
        muAx = np.einsum('ld,nltd->nlt', self.par.niw_mu0, Ax)

        #compute data contributions
        if self.periodic:
            xAx = np.einsum('nldt,nltd->nlt', self.data_wrapped, Ax)
        else:
            xAx = np.einsum('ndt,nltd->nlt', self.data, Ax)

        res = xAx - 2 * muAx + mode_const[:, None]
        return res

    def evaluate_log_emission_likelihood_NIW(self):
        E_lndetsig = self.E_ln_det_sig()
        E_musigmu = self.E_quadratic()

        lnp_tilde = - self.n_dim * np.log(np.sqrt(2 * np.pi)) + 0.5 * E_lndetsig[:, None] - 0.5 * E_musigmu
        return lnp_tilde

    def init_alpha(self):
        """Initialise first forward messages"""
        self.ln_alpha[:, :, 0] = self.loglike_emission[:, :, 0] + self.ln_G_initial[:-1]

    def init_beta(self):
        """Initialise last backward messages"""
        self.ln_beta[:, :, -1] = 0

    def update_z(self):
        """
        Set the self.alpha (forward-) and self.beta (backward-) log-messages.

        Returns
        -------
        None
        """
        # Initialize message
        self.init_alpha()
        self.init_beta()

        transition = self.ln_G[:, :-1]
        emission = self.loglike_emission

        self.ln_alpha, self.ln_beta, self.ln_zeta = JIT._fast_message_passing_normalized(self.ln_alpha, self.ln_beta, transition, emission)

        # Evaluate marginals
        self.q_z = special.softmax(self.ln_alpha + self.ln_beta, axis=1)
        self.log_q_z = np.log(self.q_z + EPS)

        unorm_log_q_zz = self.ln_alpha[:, :, None, :-1] + self.ln_G[:, :-1, None] + self.loglike_emission[:, None, :, 1:] + self.ln_beta[:, None, :, 1:]
        self.q_zz = special.softmax(unorm_log_q_zz, axis=(1, 2))
        self.log_q_zz = np.log(self.q_zz + EPS)

        #assert np.isclose(np.sum(self.q_z, axis=(1, 2)), self.n_timepoints).all(), f"Summed marginals are wrong."
        #assert np.isclose(np.sum(self.q_zz, axis=2), self.q_z[:, :, :-1]).all()

    def update_pi_transition(self):
        """
        Set the updated parameters of the variational transition distributions.

        :return: None
        """
        beta_contrib = np.tile(self.par.subordinate_gem_scaling * self.par.beta[None, :], (self.truncation_level, 1))
        sequence_contrib = np.sum(self.q_zz, axis=(0, -1))

        self.par.concentration_transition = beta_contrib
        self.par.concentration_transition[:, :-1] += (sequence_contrib + self.par.stickyness * np.eye(self.truncation_level))
        self.ln_G = self.digamma_sum(self.par.concentration_transition)

    def update_pi_initial(self):
        """
        Set the updated parameters of the variational initial distribution.

        :return: None
        """
        beta_contrib = self.par.subordinate_gem_scaling * self.par.beta
        sequence_contrib = np.sum(self.q_z[:, :, 0], axis=0)

        self.par.concentration_initial = beta_contrib
        self.par.concentration_initial[:-1] += sequence_contrib
        self.ln_G_initial = self.digamma_sum(self.par.concentration_initial)

    def update_beta(self, max_steps: int = 10, epsrel: float = 1e-3):
        """
        Update top-level stick-breaking measure beta.

        The update is a point estimate instead of a full distribution because in the DA
        truncation setting, the beta-update is no conjugate expression, c.f. Matt Johnson's
        thesis, p. 115.

        :return: None
        """
        kappa = 0.5  # decay for line-search

        # compute gradient of ELBO w.r.t. beta (Johnson's thesis, Eq. (5.4.10/11))
        def grad_L_beta(alpha_transition, alpha_initial, beta, gamma, sig):
            csum_beta_equality = np.cumsum(beta[:-1])
            csum_beta_inequality = np.zeros(beta.shape[0] - 1)
            csum_beta_inequality[1:] = csum_beta_equality[:-1]

            # tmp variable for the logs
            tmp_equality = np.log(1/(1-csum_beta_equality + EPS))
            tmp_inequality = np.log(1/(1-csum_beta_inequality + EPS))

            # to construct sum 'from i \geq k until end', reverse tmp, compute cumsum, and reverse again
            # such that the k-th element of the array holds the contribution to the k-th gradient component
            csum_log_equality = np.cumsum(tmp_equality[::-1])[::-1]
            csum_log_inequality = np.cumsum(tmp_inequality[::-1])[::-1]

            termI = 2 * csum_log_inequality
            termII = (gamma - 1) * csum_beta_equality

            #assert (np.diff(termI) <= 3 * EPS).all()
            #assert (np.diff(termII) <= 0).all()

            d_lnp = termI - termII

            # First, compute the contributions from the transition distributions
            d_E_lnp = -1 * sig * special.digamma(sig * beta[:-1] + np.eye(beta.shape[0] - 1) * self.par.stickyness) \
                      + sig * special.digamma(sig * (1 - np.sum(beta[:-1])))[None, None]\
                      + sig * special.digamma(alpha_transition[:, :-1]) \
                      - sig * special.digamma(alpha_transition[:, -1])[:, None]

            d_E_lnp = np.sum(d_E_lnp, axis=0)

            # Then add the contribution from the initial distribution
            d_E_lnp = d_E_lnp - sig * special.digamma(sig * beta[:-1]) + special.digamma(sig * (1 - np.sum(beta[:-1]))) + \
                special.digamma(alpha_initial[:-1]) - special.digamma(alpha_initial[-1])

            res = d_lnp + d_E_lnp

            return res

        ELBO_old = self.compute_ELBO()
        ELBO_new = np.NINF
        terminated = False
        n_steps = 1

        while n_steps <= max_steps and terminated is False:

            dL = grad_L_beta(self.par.concentration_transition, self.par.concentration_initial, self.par.beta,
                             self.par.top_level_gem_scaling, self.par.subordinate_gem_scaling)

            counter = 0
            max_iter = 100

            beta_old = copy.deepcopy(self.par.beta)

            while ELBO_new < ELBO_old and counter < max_iter:
                scaled_gradient = dL * kappa ** counter
                is_valid = False

                while not is_valid and counter < max_iter:
                    if (beta_old[:-1] + scaled_gradient > 0).all() and np.sum(beta_old[:-1] + scaled_gradient) < 1:
                        is_valid = True
                    else:
                        scaled_gradient = dL * kappa ** counter
                        counter += 1

                if is_valid:
                    beta_star = beta_old + 0
                    beta_star[:-1] += scaled_gradient
                    beta_star[-1] = 1 - np.sum(beta_star[:-1])
                    self.par.beta = beta_star

                    ELBO_new = self.compute_ELBO()
                    counter += 1

            if ELBO_new < ELBO_old and counter == max_iter + 1:
                self.par.beta = beta_old
                terminated = True

            if np.abs(ELBO_new - ELBO_old)/np.abs(ELBO_old) < epsrel:
                break
            elif not terminated:
                ELBO_old = copy.deepcopy(ELBO_new)
                ELBO_new = np.NINF
                n_steps += 1
                counter = 1

    def compute_loglike_GEM(self):
        csum_beta_equality_L = np.cumsum(self.par.beta[:-1])
        csum_beta_inequality_L = np.zeros(self.par.beta.shape[0] - 1)
        csum_beta_inequality_L[1:] = csum_beta_equality_L[:-1]

        den = 1 - csum_beta_inequality_L
        v = self.par.beta[:-1]/den
        v[v > 1] = 1.0  # numerics

        csum_beta_inequality_L[1 - csum_beta_inequality_L < EPS] = 1 - EPS

        ln_p = (self.par.top_level_gem_scaling - 1) * np.sum(np.log(1 - v + EPS)) + np.sum(np.log(1/(1 - csum_beta_inequality_L)))

        if np.isnan(ln_p).any():
            import ipdb; ipdb.set_trace()

        return ln_p

    @staticmethod
    def compute_KL_IW(nu1, psi1, nu2, psi2, psi1_inv=None, det_psi1=None, det_psi2=None):
        """
        Return the KL between two IW distributions, KL(IW(nu1, psi1) || IW(nu2, psi2)).
        """
        assert psi1.shape[0] == psi1.shape[1]
        assert psi2.shape[0] == psi2.shape[1]

        d = psi1.shape[0]

        if psi1_inv is None:
            psi1_inv = np.linalg.inv(psi1)
        if det_psi1 is None:
            det_psi1 = np.linalg.det(psi1)
        if det_psi2 is None:
            det_psi2 = np.linalg.det(psi2)

        J = np.arange(1, d + 1)

        return (nu1 - d - 1)/2 * np.log(det_psi1) - (nu2 - d -1)/2 * np.log(det_psi2) \
    + np.sum(special.loggamma((nu2 - d - J)/2) - special.loggamma((nu1 - d - J)/2)) \
    + (nu2 - nu1)/2 * (np.log(det_psi1) - np.sum(special.digamma((nu1 - d - J)/2))) \
    + np.trace(-(nu1 - d - 1)/2 * (psi1_inv @ (psi1 - psi2)))

    @staticmethod
    def compute_Dir_entropy(alpha):
        """
        Return the entropy of multiple Dirichlets with the concentration parameters alpha.

        :param alpha: [M, K] array (corresponding to M Dirichlet distributions)
        :return: [M,] array
        """
        log_B = np.sum(special.loggamma(alpha), axis=1) - special.loggamma(np.sum(alpha, axis=1))
        a_0 = np.sum(alpha, axis=1)
        K = alpha.shape[1]

        return log_B + (a_0 - K) * special.digamma(a_0) - np.sum((alpha - 1) * special.digamma(alpha), axis=1)

    @staticmethod
    def compute_KL_Dir(alpha, beta):
        """
        Compute the KL divergence between two Dirichlets.

        :param alpha: [K,] array
        :param beta: [K,] array
        :return: float
        """
        a0 = np.sum(alpha)
        b0 = np.sum(beta)
        return special.loggamma(a0) - special.loggamma(b0) - np.sum(special.loggamma(alpha)) \
               + np.sum(special.loggamma(beta)) + np.sum((alpha - beta) * (special.digamma(alpha) - special.digamma(a0)))

    def compute_ELBO(self):
        """
        As the name says
        """
        loglike_GEM = self.compute_loglike_GEM()
        self.loglike_GEM.append(loglike_GEM)
        neg_KL_IW_modes = [-1 * self.compute_KL_IW(self.par.iw_dof[i], self.par.iw_scale[i], self.hpar.iw_dof[i], self.hpar.iw_scale[i],
                                                   psi1_inv=self.iw_scale_inv[i], det_psi1=1 / (self.det_iw_scale_inv[i]),
                                                   det_psi2=self.hpar.det_iw_scale[i]) for i in range(self.truncation_level)]
        neg_KL_NIW = np.sum(neg_KL_IW_modes) + self.n_dim / 2 * np.sum(1 - self.hpar.niw_lambda / self.par.niw_lambda - np.log(self.par.niw_lambda / self.hpar.niw_lambda))
        quadratic_form = [(self.hpar.niw_mu0[i] - self.par.niw_mu0[i]) @ self.iw_scale_inv[i] @ (self.hpar.niw_mu0[i] - self.par.niw_mu0[i])
                          for i in range(self.truncation_level)]
        neg_KL_NIW -= np.sum(self.hpar.niw_lambda * self.par.iw_dof * quadratic_form)
        self.neg_KL_NIW.append(neg_KL_NIW)

        prior_conc = np.tile(self.par.beta * self.par.subordinate_gem_scaling, (self.truncation_level, 1))
        #prior_conc[:, :-1] += self.par.kappa * np.eye(self.truncation_level)  TODO stickyness
        neg_KL_Dir_modes = [self.compute_KL_Dir(self.par.concentration_transition[i], prior_conc[i]) for i in range(self.truncation_level)]
        neg_KL_Dir = -1 * np.sum(neg_KL_Dir_modes)
        neg_KL_Dir_init = -1 * self.compute_KL_Dir(self.par.concentration_initial, self.par.beta * self.par.subordinate_gem_scaling)
        self.neg_KL_Dir.append(neg_KL_Dir)
        self.neg_KL_Dir_init.append(neg_KL_Dir_init)
        self.log_Z.append(np.sum(self.ln_zeta))

        L = loglike_GEM + neg_KL_NIW + neg_KL_Dir + neg_KL_Dir_init + np.sum(self.ln_zeta)

        if len(self.ELBO) > 1:
            if L < self.ELBO[-1]:
                if not np.isclose(L, self.ELBO[-1]):
                    pass
                    #import ipdb; ipdb.set_trace()
        if np.isnan(L):
            import ipdb; ipdb.set_trace()

        return L

    def update_kappa(self):
        grad_ki = special.digamma(self.par.sig + self.par.kappa) \
                  - special.digamma(self.par.sig*self.par.beta[:-1] + self.par.kappa) \
                  + special.digamma(np.diag(self.par.concentration_transition)) \
                  - special.digamma(np.sum(self.par.concentration_transition, axis=1))

        epsilon = 0.8
        is_valid = False
        while not is_valid:
            if ((self.par.kappa + grad_ki) >= 0).all():
                is_valid = True
            else:
                grad_ki *= epsilon

        self.par.kappa += grad_ki

    def step(self):
        """
        Run one CAVI update for every q-distribution.

        :return: None
        """
        self.update_z()  # latent sequences
        L = self.compute_ELBO()
        self.ELBO.append(L)
        self.update_beta()
        self.update_pi_transition()  # transition distribution
        self.update_pi_initial()  # initial distribution
        self.update_NIW()  # emission distribution
        #self.update_kappa()  # stickiness

    def fit(self, epsrel: float = 1e-3, checkpoint_interval: int = -1):
        """
        Performs a CAVI algorithms on the model.

        :param epsrel: convergence criterion, relative ELBO change between iterations
        :param checkpoint_interval: each checkpoint_interval iteration the object is saved to the checkpoint path (off if negative)
        :return:
        """
