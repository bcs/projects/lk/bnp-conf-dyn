"""
Collection of helper function that make use of numba-JIT compiling.
"""
from numba import jit
import numpy as np

@jit(nopython=True)
def logsumexp(log_x, axis):
    """
    Return logsumexp of <log_x> along axis <axis>. ATTENTION: note the difference between `logsumexp' and `logsumexp2'
    in the expected argument shape.

    Note: basically copied from scipy source code, had to be adapted however to be numba-compatible (e.g., no use of
    summing over multiple axis simultaneously).

    Args:
        log_x : array, (k, l)
        axis : int
            Specifies the axis along which to compute the sum.

    Returns:
        res : (k,) or (l,), depending on <axis>.

    """
    if axis == 0:
        a_max = np.zeros((1, log_x.shape[1]))
        i = 0
        for col_id in range(log_x.shape[1]):
            m = np.amax(log_x[:, col_id])
            if np.isfinite(m):
                a_max[0, i] = m
            else:
                a_max[0, i] = 0
            i += 1

    elif axis == 1:
        a_max = np.zeros((log_x.shape[0], 1))
        i = 0
        for row_id in range(log_x.shape[0]):
            m = np.amax(log_x[row_id])
            if np.isfinite(m):
                a_max[i, 0] = m
            else:
                a_max[i, 0] = 0
            i += 1

    else:
        raise ValueError

    tmp = np.exp(log_x - a_max)

    s = np.sum(tmp, axis=axis)
    out = np.log(s)

    a = np.sum(a_max, axis=axis)
    out += a

    return out


@jit(nopython=True)
def logsumexp2(log_x, axis):
    """
    Return logsumexp of <log_x> along axis <axis>. ATTENTION: note the difference between `logsumexp' and `logsumexp2'
    in the expected argument shape.

    Note: basically copied from scipy source code, had to be adapted however to be numba-compatible (e.g., no use of
    summing over multiple axis simultaneously).

    Args:
        log_x : array, (k, l, m)
        axis : int
            Specifies the axis along which to compute the sum.

    Returns:
        res : (k, l) or (k, m), depending on <axis>.

    """
    N = log_x.shape[0]

    if axis == 1:
        a_max = np.zeros((N, 1, log_x.shape[2]))
        n = 0
        for seq_id in range(N):

            i = 0
            for col_id in range(log_x.shape[2]):
                m = np.amax(log_x[seq_id, :, col_id])
                if np.isfinite(m):
                    a_max[n, 0, i] = m
                else:
                    a_max[n, 0, i] = 0
                i += 1

            n += 1

    elif axis == 2:
        a_max = np.zeros((N, log_x.shape[1], 1))

        n = 0
        for seq_id in range(N):

            i = 0
            for row_id in range(log_x.shape[1]):
                m = np.amax(log_x[n, row_id])
                if np.isfinite(m):
                    a_max[n, i, 0] = m
                else:
                    a_max[n, i, 0] = 0
                i += 1

            n += 1

    else:
        raise ValueError

    tmp = np.exp(log_x - a_max)

    s = np.sum(tmp, axis=axis)
    out = np.log(s)

    a = np.sum(a_max, axis=axis)
    out += a

    return out


@jit(nopython=True)
def _fast_message_passing(ln_alpha, ln_beta, log_transition, log_emission):
    """
    Return the forward- and backward messages of a HMM with ln_alpha.shape[0] sequences. Messages for all
    sequences are computed in parallel, i.e., this is to be used in conventional VI (and *not* in Collapsed VI).
    For the latter, choose _fast_message_passing_CVI.

    Note that ln_alpha and ln_beta have to be provided; the first and last
    element have to be properly initialized.

    :param ln_alpha: array of shape (N_sequences, N_modes, N_timepoints)
    :param ln_beta: array of shape (N_sequences, N_modes, N_timepoints)
    :param log_transition: array of shape (N_modes, N_modes)
    :param log_emission: array of shape (N_sequences, N_modes, N_timepoints)

    :return ln_alpha: array, shape s.a., contains messages
    :return ln_beta: array, shape s.a., contains messages
    """
    N, L, T = ln_alpha.shape

    # Forward/alpha recursion
    for t in range(1, T):
        exponent = np.expand_dims(ln_alpha[:, :, t - 1], axis=2) + log_transition
        ln_alpha[:, :, t] = log_emission[:, :, t] + logsumexp2(exponent, 1)

    # Backward/beta recursion
    for t in range(T - 2, -1, -1):
        exponent = log_transition + np.expand_dims(log_emission[:, :, t + 1], axis=1) + np.expand_dims(ln_beta[:, :, t + 1], axis=1)
        ln_beta[:, :, t] = logsumexp2(exponent, 2)

    return ln_alpha, ln_beta


@jit(nopython=True)
def _fast_message_passing_normalized(ln_alpha, ln_beta, log_transition, log_emission):
    """
    As _fast_message_passing, but with the alpha-messages normalized s.t. they represent conditional
    probabilities, a_t = p(z_t| x_1, .., x_t).
    """
    N, L, T = ln_alpha.shape
    ln_zeta = np.zeros((N, T))
    ln_zeta[:, 0] = logsumexp(ln_alpha[:, :, 0], 1)
    ln_alpha[:, :, 0] = ln_alpha[:, :, 0] - np.expand_dims(ln_zeta[:, 0], axis=1)

    # Forward/alpha recursion
    for t in range(1, T):
        exponent = np.expand_dims(ln_alpha[:, :, t - 1], axis=2) + log_transition
        ln_alpha[:, :, t] = log_emission[:, :, t] + logsumexp2(exponent, 1)
        ln_zeta[:, t] = logsumexp(ln_alpha[:, :, t], 1)
        ln_alpha[:, :, t] = ln_alpha[:, :, t] - np.expand_dims(ln_zeta[:, t], axis=1)

    # Backward/beta recursion
    for t in range(T - 2, -1, -1):
        exponent = log_transition + np.expand_dims(log_emission[:, :, t + 1], axis=1) + np.expand_dims(ln_beta[:, :, t + 1], axis=1)
        ln_beta[:, :, t] = logsumexp2(exponent, 2)

    return ln_alpha, ln_beta, ln_zeta


@jit(nopython=True)
def _fast_message_passing_CVI(ln_alpha, ln_beta, log_transition, log_emission):
    """
    Return the forward- and backward messages of one sequence as well as the log marginals.
    This has to be used for CVI, because there, one cannot compute the messages for all sequences simultaneously
    since they are coupled. Additionally, log_transition and log_emission are not the log transition- and emission
    probabilities, but rather effective quantities that derive from these. Hence, they have (in general) different
    values for every transition and every emission and hence are also arrays of length N_timepoints, as are the
    log messages.

    IMPORTANT: the documentation for transition and emission needs to be updated and the method double-checked.
    Until then, this method will throw an error.

    Note that ln_alpha and ln_beta have to be provided; the first and last
    element have to be properly initialized.

    :param ln_alpha: array of shape (N_sequences, N_modes, N_timepoints)
    :param ln_beta: array of shape (N_sequences, N_modes, N_timepoints)
    :param log_transition: array of shape (N_modes, N_modes)
    :param log_emission: array of shape (N_modes, N_observation_dimension)

    :return ln_alpha: array, shape s.a., contains messages
    :return ln_beta: array, shape s.a., contains messages
    """
    raise PermissionError  # DOUBLE CHECK THE TRANSITION AND EMISSION SHAPES
    T = ln_alpha.shape[1]

    # Forward recursion
    for t in range(1, T):
        #exponent = ln_alpha[:, None, t - 1] + log_xi
        exponent = (ln_alpha[:, t - 1] + log_transition.T).T
        ln_alpha[:, t] = log_emission[:, t] + logsumexp(exponent, axis=0)

    # Backward recursion
    for t in range(T - 2, -1, -1):
        exponent = log_transition + log_emission[:, t + 1] + ln_beta[:, t + 1]
        ln_beta[:, t] = logsumexp(exponent, axis=1)

    # Evaluate marginals
    log_q_z_i = ln_alpha + ln_beta
    normaliser_z_i = logsumexp(log_q_z_i, axis=0)
    log_q_z_i -= normaliser_z_i

    #unorm_log_q_zz_i = ln_alpha[:, None, :-1] + log_xi[:, :, None] + log_zeta[None, :, emission_indices[1:]] + ln_beta[None, :, 1:]
    #normaliser_zz_i = sp.logsumexp(log_q_zz_i, axis=(0, 1))
    #log_q_zz_i -= normaliser_zz_i[None, None, :]

    return log_q_z_i, ln_alpha, ln_beta

