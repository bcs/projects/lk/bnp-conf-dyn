import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate as integrate
import scipy.stats as stats
import scipy.special as special

def bivariate_vM(phi, psi, mu, nu, kappa):
    """
    Function proportional to the bivariate von-Mises density.

    Args:
        phi: angle, in [0, 2pi)
        psi: angle, in [0, 2pi)
        mu: `location' of phi
        nu: `location' of psi
        kappa: array, (3,), holding kappa1, kappa2, kappa3
    Returns:

    """
    return np.exp(kappa[0]*np.cos(phi - mu) + kappa[1]*np.cos(psi - nu) - kappa[2]*np.cos(phi - mu - psi + nu))


def bivariate_vM_marginal(psi, k1, k2, k3, nu):
    """
    Function proportional to the 1D-marginal of the bivariate von-Mises (Eq. 13 in Supplementary of Boomsma 2009).

    Args:
        psi: [0, 2pi)
        k1,2,3: concentration parameters
        nu: `location' of psi

    Returns:

    """
    k13 = np.sqrt(k1**2 + k3**2 - 2*k1*k3*np.cos(psi - nu))
    return special.iv(0, k13) * np.exp(k2 * np.cos(psi - nu))


def sample_BvM(mu, kappa, n_samples=1):
    """
    Return "n_samples" from a Bivariate von Mises distribution with location mu and concentration
    parameters kappa.

    :param mu: [2,]
    :param kappa: [3,]
    :param n_samples: int
    :return: [n_samples, 2]
    """
    # Discretize [0, 2pi] to easily find the factor M for rejection sampling
    N_mesh = 500
    x = np.linspace(0, 2*np.pi, N_mesh)

    normalizer_bivariate, _ = integrate.dblquad(bivariate_vM, -np.pi, np.pi, -np.pi, np.pi, args=(*mu, kappa))

    # Use a 1D-vM with location nu (location of psi) and concentration k3 as proposal distribution
    f = lambda y: 2 * np.pi * bivariate_vM_marginal(y, *kappa, mu[1])/normalizer_bivariate  # True distribution
    g = lambda y, kappa: stats.vonmises.pdf(y, kappa, loc=mu[1])  # Proposal distribution

    # find factor M
    idx = np.argmax(f(x)/g(x, kappa[2]))
    M = f(x[idx])/g(x[idx], kappa[2])

    k_log = np.logspace(-1, 2, 500)
    max_dist = np.zeros(len(k_log))
    M = np.zeros(len(k_log))
    for i, kl in enumerate(k_log):
        idx = np.argmax(f(x) / g(x, kl))
        M[i] = f(x[idx]) / g(x[idx], kl)
        max_dist[i] = np.max(np.abs(M[i] * g(x, kl) - f(x)))

    min_max_dist_idx = np.argmin(max_dist)
    M = M[min_max_dist_idx]
    k = k_log[min_max_dist_idx]

    psi_samples = stats.vonmises(k, loc=mu[1]).rvs(n_samples)
    u = np.random.rand(n_samples)
    indices_to_keep = np.zeros(n_samples)
    for i in range(n_samples):
        print(i)
        if u[i] * g(psi_samples[i], k) * M < f(psi_samples[i]):
            indices_to_keep[i] = 1  # this is faster than appending to a list the accepted samples
        else:
            pass
    psi_samples = psi_samples[indices_to_keep.astype(bool)]

    # Sample from f(phi | psi), which is vM(nu_psi, k13) with k13 as in marginal_bivariatevM and
    # tan(nu_psi) = -k3 sin(psi - nu) / (k1 - k3 * cos(psi - nu))
    nu_psi = np.arctan(kappa[2] * np.sin(psi_samples - mu[1]) / (kappa[0] - kappa[2] * np.cos(psi_samples - mu[1])))
    mu_tilde = mu[0] - nu_psi
    k13 = np.sqrt(kappa[0] ** 2 + kappa[2] ** 2 - 2 * kappa[0] * kappa[2] * np.cos(psi_samples))
    phi_samples = stats.vonmises(k13, loc=mu_tilde).rvs(len(psi_samples))

    return np.array([phi_samples, psi_samples])
    #plt.plot(x, 2 * np.pi * bivariate_vM_marginal(x, *kappa, mu[1]) / normalizer_bivariate, label='true dist')
    #plt.plot(x, M * g(x, k), label='proposal dist')
    #plt.legend()
    #plt.show()

np.random.seed(1)
N = 10
T = 1000
D = 2

A_pos = np.array([[0.99, 0.01, 0],
                  [0, 0.99, 0.01],
                  [0.01, 0, 0.99]])
pi_pos = np.array([1/3, 1/3, 1/3])

mu = np.zeros((3, 2))
mu[0] = np.array([1.0 * np.pi, 0.1 * np.pi])
mu[1] = np.array([0.6 * np.pi, 0.4 * np.pi])
mu[2] = np.array([1.98 * np.pi, 1.5 * np.pi])
kappa = np.array([[20, 3],
                  [3, 30]])
kappa3 = np.array([10, 5, 2])

BvM_samples = sample_BvM(mu[2], kappa3, n_samples=10000)
bvm_pointer = 0

z_pos = np.zeros((N, T))
x_pos = np.zeros((N, D, T))
for n in range(N):
    z_pos[n, 0] = np.random.choice([0, 1, 2], p=pi_pos)
    if z_pos[n, 0] in [0, 1]:
        x_pos[n, 0, 0] = stats.vonmises(kappa[int(z_pos[n, 0]), 0], loc=mu[int(z_pos[n, 0]), 0]).rvs()
        x_pos[n, 1, 0] = stats.vonmises(kappa[int(z_pos[n, 0]), 1], loc=mu[int(z_pos[n, 0]), 1]).rvs()
    else:
        x_pos[n, :, 0] = BvM_samples[:, bvm_pointer]
        bvm_pointer += 1

    for t in range(1, T):
        print(t)
        z_pos[n, t] = np.random.choice([0, 1, 2], p=A_pos[int(z_pos[n, t-1])])
        if z_pos[n, t] in [0, 1]:
            x_pos[n, 0, t] = stats.vonmises(kappa[int(z_pos[n, t]), 0], loc=mu[int(z_pos[n, t]), 0]).rvs()
            x_pos[n, 1, t] = stats.vonmises(kappa[int(z_pos[n, t]), 1], loc=mu[int(z_pos[n, t]), 1]).rvs()
        else:
            x_pos[n, :, t] = BvM_samples[:, bvm_pointer]
            bvm_pointer += 1

wrap_idx_right = np.where(x_pos > 2 * np.pi)
x_pos[wrap_idx_right] = x_pos[wrap_idx_right] % (2 * np.pi)
wrap_idx_left = np.where(x_pos < 0)
x_pos[wrap_idx_left] += 2 * np.pi

np.save(f'../data/3state_vonMises_hmm/input/x_pos_{N}_{D}_{T}', x_pos)
np.save(f'../data/3state_vonMises_hmm/input/z_pos_{N}_{D}_{T}', z_pos)
