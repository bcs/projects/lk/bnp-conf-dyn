"""
Generate synthetic data from 3-well-potential according to Wu et al., JCP, 2017.
"""
from mpl_toolkits.mplot3d import Axes3D  # for 2D-surface of potential
from matplotlib import cm
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()


def U(x, y):
    """
    Potential as defined in Wu et al.

    Parameter
    ---------
    x, y : float
        2D-position
    """
    return (3 * np.exp(-x**2 - (y - 1/3)**2) - 3 * np.exp(-x**2 - (y - 5/3)**2) - 5 * np.exp(-(x-1)**2 - y**2)
            - 5 * np.exp(-(x+1)**2 - y**2) + 1/5 * x**4 + 1/5 * (y - 1/3)**4)


def grad_U(x, y):
    """
    Gradient of potential.
    """
    dx = (3 * np.exp(-x**2 - (y - 1/3)**2) * -2 * x - 3 * np.exp(-x**2 - (y - 5/3)**2) * -2 * x
          -5 * np.exp(-(x-1)**2 - y**2) * -2 * (x-1) - 5 * np.exp(-(x+1)**2 - y**2) * -2 * (x+1)
          + 4/5 * x**3)
    dy = (3 * np.exp(-x**2 - (y - 1/3)**2) * -2 * (y - 1/3) - 3 * np.exp(-x**2 - (y - 5/3)**2) * -2 * (y - 5/3)
          - 5 * np.exp(-(x-1)**2 - y**2) * -2 * y - 5 * np.exp(-(x+1)**2 - y**2) * -2 * y
          + 4/5 * (y - 1/3)**3)
    res = np.array([dx, dy])
    return res


N = 3  # number of trajectories
T = 10000  # number of time points per trajectory

x = np.linspace(-2, 2, 200)
y = np.linspace(-1, 2, 200)
X, Y = np.meshgrid(x, y)

#from matplotlib import cm
#plt.contourf(X, Y, U(X, Y), levels=15, cmap=cm.jet)
#plt.xlabel('x')
#plt.ylabel('y')
#plt.savefig('../plots/energy_landscape.pdf', format='pdf')

# Plot 3D-surface
fig = plt.figure()
ax = fig.gca(projection='3d')

surf = ax.plot_surface(X, Y, U(X, Y), cmap=cm.coolwarm)
plt.show()

import sys; sys.exit()
beta = 1.6
dt = 0.05
x = np.zeros((N, T, 2))

for n in range(N):
    x[n, 0, 0] = np.random.uniform(-2, -1.5)
    x[n, 0, 1] = np.random.uniform(-1.5, 2.5)

    for t in range(1, T):
        dx = -1 * grad_U(x[n, t - 1, 0], x[n, t - 1, 1]) * dt + np.sqrt(2/beta) * np.random.normal(loc=0, scale=np.sqrt(dt), size=2)
        x[n, t] = x[n, t - 1] + dx

x = np.einsum('ntd->ndt', x)  # required input format for VarDist-class
import time
t = time.localtime()
timestamp = time.strftime("%d-%m-%Y_%H-%M-%S", t)
np.save(f'../data/3well_SDE/input/x_{N}_{T}', x)
#x_all = np.concatenate([x[i] for i in range(N)], axis=-1)
#import sys
#sys.exit()
#sns.jointplot(x=x_all[0, :], y=x_all[1, :], kind='kde')
#plt.xlabel("x")
#plt.ylabel("y")
#plt.show()
#plt.xlim((-2, 2))
#plt.ylim((-2, 2))
#plt.show()

