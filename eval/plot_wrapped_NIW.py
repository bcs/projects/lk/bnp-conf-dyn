import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import tables
from matplotlib.patches import Ellipse
import matplotlib.transforms as transforms
matplotlib.rcParams['axes.linewidth'] = 1.5 # set the value globally
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['mathtext.rm'] = 'serif'

def confidence_ellipse(cov, mu, ax, n_std=3.0, facecolor='none', **kwargs):
    """
    Create a plot of the covariance confidence ellipse of *x* and *y*.

    Parameters
    ----------
    cov : array-like, shape (2, 2)
        Covariance matrix.

    ax : matplotlib.axes.Axes
        The axes object to draw the ellipse into.

    n_std : float
        The number of standard deviations to determine the ellipse's radiuses.

    Returns
    -------
    matplotlib.patches.Ellipse

    Other parameters
    ----------------
    kwargs : `~matplotlib.patches.Patch` properties
    """
    pearson = cov[0, 1] / np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensionl dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0),
                      width=ell_radius_x * 2,
                      height=ell_radius_y * 2,
                      facecolor=facecolor,
                      **kwargs)

    # Calculating the stdandard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = n_std * np.sqrt(cov[0, 0])
    mean_x = mu[0]

    # calculating the stdandard deviation of y ...
    scale_y = n_std * np.sqrt(cov[1, 1])
    mean_y = mu[1]

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)


# Specify data paths
RES_PATH = '../data/3state_vonMises_hmm/output/'
#TIMESTAMP = '25-08-2021_14-17-24'
TIMESTAMP = '10-09-2021_10-41-34'
FNAME = TIMESTAMP + '_results.h5'

data_x = np.load('../data/3state_vonMises_hmm/input/x_pos_10_2_1000.npy')
data_z = np.load('../data/3state_vonMises_hmm/input/z_pos_10_2_1000.npy')
N_datasets = data_x.shape[0]
N = data_x.shape[0]
T = data_x.shape[2]

# Plot the raw data
def plot_raw():
    N = data_x.shape[1]
    T = data_x.shape[3]
    c = ['r', 'g', 'b']
    for n in range(N):
        for t in range(T):
            plt.scatter(data_x[1, n, 0, t], data_x[1, n, 1, t], c=c[int(data_z[1, n, t])], alpha=.4)
    plt.xticks([0, np.pi/2, np.pi, 3/2 * np.pi, 2 * np.pi],
               ['-$\pi$', '-$\pi/2$', '0', '$\pi/2$', '$\pi$'])
    plt.yticks([0, np.pi/2, np.pi, 3/2 * np.pi, 2 * np.pi],
               ['-$\pi$', '-$\pi/2$', '0', '$\pi/2$', '$\pi$'])
    plt.xlabel("$\phi$")
    plt.ylabel("$\psi$")
    plt.tight_layout()
    plt.subplots_adjust(left=0.12, right=0.99, top=0.99, bottom=0.12)
    plt.savefig('../plots/3state_vonMises_hmm/raw_data_scatter.pdf', format='pdf')
    plt.close()


# Plot one reconstruction of the latent sequence together with the ground truth
def plot_q_z():

    h5file = tables.open_file(RES_PATH + FNAME)

    ELBO, run_lengths = [], []
    for run_id, run in enumerate(h5file.root.ELBO):
        ELBO.append(np.array(run))
        ELBO_final = [L[-1] for L in ELBO]
        run_lengths.append(len(np.array(run)) - 1)
    run_max_L = np.argmax(ELBO_final)

    q_z_final = np.array(h5file.root.q_z[f"run_{run_max_L}"][f'iteration_{run_lengths[run_max_L]}'])
    h5file.close()

    #fig, ax = plt.subplots(2, 2, sharex=True, gridspec_kw={'hspace': 0.15, 'height_ratios': [1, 3], 'width_ratios':[9, 1]}, figsize=(5, 2.5))

    widths = [30, 1]
    heights = [1, 2.8]
    fig = plt.figure(figsize=(5, 1.8))
    gs = gridspec.GridSpec(ncols=2, nrows=2, figure=fig, width_ratios=widths, height_ratios=heights, wspace=0.03,
                           hspace=0.12)
    ax1 = fig.add_subplot(gs[0, 0])
    ax3 = fig.add_subplot(gs[1, 0], sharex=ax1)
    ax4 = fig.add_subplot(gs[1, 1])
    plt.setp(ax1.get_xticklabels(), visible=False)  # No x-ticklabels on q plot

    z_trajectory = data_z[1] + 1

    ax1.plot(z_trajectory.astype('int'), lw=2)
    ax1.set_ylim((0.7, 3.3))
    ax1.set_yticks([1, 2, 3])
    ax1.set_yticklabels([3, 2, 1])
    mat = ax3.matshow(q_z_final[1], aspect='auto', cmap='Blues')

    #ax[0].set_ylabel('Latent state')
    ax1.set_ylabel('$Z_{\mathrm{true}}$', fontsize='x-large')

    ax3.set_xlabel('Time', fontdict={'size': 'x-large', 'name': 'cmr10'})
    ax3.xaxis.set_label_coords(0.5, -0.25)
    #ax[1].set_ylabel('Latent state')
    ax3.xaxis.set_ticks_position('bottom')
    ax3.set_ylabel('$Z$', fontsize='x-large')
    ax3.tick_params(axis='x', top=False, labelbottom=True, labeltop=False)
    ax3.set_xticks([0, 200, 400, 600, 800, 1000])
    ax3.set_xticklabels([0, 200, 400, 600, 800, 1000])
    ax3.set_yticks([0, 2, 4, 6, 8])
    ax3.set_yticklabels([1, 3, 5, 7, 9])
    ax3.set_ylim([9.5, -0.5])

    plt.colorbar(mat, ax4, ticks=[0, 1])
    plt.subplots_adjust(left=0.09, right=0.96, top=0.975, bottom=0.23, hspace=-0.60)
    plt.savefig(f'../plots/3state_vonMises_hmm/q_z_reconstruction_{TIMESTAMP}.pdf', format='pdf')
    plt.close()

# Plot the `fill states' of the latent modes
def plot_n_modes():
    h5file = tables.open_file(RES_PATH + FNAME)
    n_modes = np.array(h5file.root.modes_vs_iterations)
    h5file.close()
    n_iter = n_modes.shape[-1]
    modes_mean = np.mean(n_modes, axis=0)
    #modes_std = np.std(n_modes, axis=0)
    fig, ax = plt.subplots()
    for trace in n_modes:
        ax.plot(np.arange(1, n_iter + 1), trace, lw=.9)
    #plt.fill_between(np.arange(1, n_iter + 1), modes_mean - modes_std, modes_mean + modes_std, alpha=.3)
    ax.plot(np.arange(1, n_iter + 1), modes_mean, c='r', lw=3, alpha=.6)
    plt.xlabel('Iteration')
    plt.ylabel('Number of relevant latent states')
    #plt.xlim((0, 2000))
    plt.ylim((0, 11))
    plt.xticks([0, 500, 1000, 1500, 2000], [0, 500, 1000, 1500, 2000])
    plt.yticks(range(11), range(11))
    ax.set_aspect(110)
    plt.tight_layout()
    plt.subplots_adjust(left=0.1, right=0.99, top=0.99, bottom=0.0)
    plt.savefig('../plots/3state_vonMises_hmm/modes_vs_iter_mean_NIW_' + TIMESTAMP + '.pdf', format='pdf')
    plt.close()

def print_transition_probabilities():
    h5file = tables.open_file(RES_PATH + FNAME)
    alpha_transition = np.array(h5file.root.concentration_transition.run_0['iteration_499'])
    h5file.close()

    print(np.around(alpha_transition/np.sum(alpha_transition, axis=1)[:, None], 2))


def plot_posterior_NIW():

    h5file = tables.open_file(RES_PATH + FNAME)

    ELBO, run_lengths = [], []
    for run_id, run in enumerate(h5file.root.ELBO):
        ELBO.append(np.array(run))
        ELBO_final = [L[-1] for L in ELBO]
        run_lengths.append(len(np.array(run)))
    run_max_L = np.argmax(ELBO_final)
    run_length = run_lengths[run_max_L] - 1

    mu = np.array(h5file.root.niw_mu0[f"run_{run_max_L}"][f'iteration_{run_length}'])
    nu = np.array(h5file.root.iw_dof[f"run_{run_max_L}"][f'iteration_{run_length}'])
    lambd = np.array(h5file.root.niw_lambda[f"run_{run_max_L}"][f'iteration_{run_length}'])
    psi = np.array(h5file.root.iw_scale[f"run_{run_max_L}"][f'iteration_{run_length}'])
    q_z_final = np.array(h5file.root.q_z[f"run_{run_max_L}"][f'iteration_{run_length}'])
    h5file.close()

    ####################
    import matplotlib.colors as colors
    # identify three most `filled' modes (as I know the model result = 3 modes already)
    indices = np.argsort(np.sum(q_z_final, axis=(0, 2)))[-3:]

    mu_true = np.array([[1.0 * np.pi, 0.1 * np.pi],
                       [0.6 * np.pi, 0.4 * np.pi],
                       [1.98 * np.pi, 1.5 * np.pi]])

    fig, ax = plt.subplots(1, 2, sharey=True, gridspec_kw={'wspace': 0.05, 'width_ratios': [1, 1]}, figsize=(5, 2.5))
    # Plot ground-truth
    T = data_x.shape[2]
    c = ['tab:green', 'tab:red', 'tab:blue']

    ax[0].scatter(data_x[0, 0, :], data_x[0, 1, :], c=np.array(c)[data_z[0, :].astype(int)], alpha=1, s=10)

    #rgba_r = colors.to_rgba('tab:red')
    #rgba_g = colors.to_rgba('tab:green')
    #rgba_b = colors.to_rgba('tab:blue')
    #rgba_qr = np.zeros((T, 4))
    #rgba_qr[:] = rgba_r
    #rgba_qg = np.zeros((T, 4))
    #rgba_qg[:] = rgba_g
    #rgba_qb = np.zeros((T, 4))
    #rgba_qb[:] = rgba_b

    #rgba_qr[:, 3] = q_z_final[0, indices[0], :].flatten()
    #rgba_qg[:, 3] = q_z_final[0, indices[1], :].flatten()
    #rgba_qb[:, 3] = q_z_final[0, indices[2], :].flatten()

    #ax[1].scatter(data_x[0, 0, :], data_x[0, 1, :], c=rgba_qb, s=10)
    #ax[1].scatter(data_x[0, 0, :], data_x[0, 1, :], c=rgba_qg, s=10)
    #ax[1].scatter(data_x[0, 0, :], data_x[0, 1, :], c=rgba_qr, s=10)
    #for i in indices:
    #    # Reconstructions
    #    ax[1].scatter(mu[i, 0], mu[i, 1], marker='d', c='aqua', s=140, alpha=1)
    #    confidence_ellipse(psi[i]/(nu[i] - 3), mu[i], ax[1], n_std=1, edgecolor='aqua', alpha=1, lw=2.5)

    map_indices = np.argmax(q_z_final, axis=1)
    index_set, occurrences = np.unique(map_indices.flatten(), return_counts=True)
    index_set = index_set[occurrences > N * T * 0.001]
    occurrences = occurrences[occurrences > N * T * 0.001]
    c = ['tab:red', 'tab:green', 'tab:blue']
    for idx, i in enumerate(index_set):
        tmp = data_x[0, :, (map_indices == i)[0]]
        ax[1].scatter(tmp[:, 0], tmp[:, 1], c=c[idx], alpha=1, s=10)

    for i in range(3):
        # Ground truth
        ax[1].scatter(mu_true[i, 0], mu_true[i, 1], marker='d', c='cyan', s=80, alpha=1)

    q = q_z_final.sum((0, 2))/(q_z_final.shape[0] * q_z_final.shape[2])
    for i in range(10):
        # Reconstructions
        if q[i] > 0.01:
            ax[1].scatter(mu[i, 0], mu[i, 1], marker='x', c='k', s=80, alpha=1, zorder=10)
            confidence_ellipse(psi[i]/(nu[i] - 3), mu[i], ax[1], n_std=1, edgecolor='k', alpha=1, lw=2)


    plt.xlim((-0.1 * np.pi, 2.1 * np.pi))
    plt.ylim((-0.1 * np.pi, 2.1 * np.pi))
    ax[0].set_xticks([0, np.pi, 2 * np.pi])
    ax[0].set_xticklabels(['-$\pi$', '0', '$\pi$'])
    ax[1].set_xticks([0, np.pi, 2 * np.pi])
    ax[1].set_xticklabels(['-$\pi$', '0', '$\pi$'])
    ax[0].set_yticks([0, np.pi, 2 * np.pi])
    ax[0].set_yticklabels(['-$\pi$', '0', '$\pi$'])
    ax[0].set_xlabel('$\phi$', fontsize='x-large')
    ax[1].set_xlabel('$\phi$', fontsize='x-large')
    ax[0].set_ylabel('$\psi$', fontsize='x-large')
    ax[0].yaxis.set_label_coords(-0.1, 0.48)
    ax[1].xaxis.set_label_coords(0.52, -0.12)
    ax[0].xaxis.set_label_coords(0.52, -0.12)


    plt.tight_layout()
    plt.subplots_adjust(left=0.08, right=0.99, top=0.99, bottom=0.18)
    plt.savefig(f'../plots/3state_vonMises_hmm/posterior_means_{TIMESTAMP}.pdf', format='pdf')
    plt.close()

def plot_ELBO():
    """
    Plot the ELBO trajectories of all simulations.
    """

    h5file = tables.open_file(RES_PATH + FNAME, 'a')
    ELBO = []
    for run_id, run in enumerate(h5file.root.ELBO):
        print(run_id)
        ELBO.append(np.array(run.iteration_99))
    h5file.close()

    for trajectory in ELBO:
        plt.plot(trajectory[2:])

    plt.show()
    pass


#plot_ELBO()
plot_q_z()
#plot_n_modes()
plot_posterior_NIW()
#plot_raw()
