import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import tables
from matplotlib.patches import Ellipse
import matplotlib.transforms as transforms

matplotlib.rcParams['axes.linewidth'] = 1.5 # set the value globally
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['mathtext.rm'] = 'serif'

def confidence_ellipse(cov, mu, ax, n_std=3.0, facecolor='none', **kwargs):
    """
    Create a plot of the covariance confidence ellipse of *x* and *y*.

    Parameters
    ----------
    cov : array-like, shape (2, 2)
        Covariance matrix.

    ax : matplotlib.axes.Axes
        The axes object to draw the ellipse into.

    n_std : float
        The number of standard deviations to determine the ellipse's radiuses.

    Returns
    -------
    matplotlib.patches.Ellipse

    Other parameters
    ----------------
    kwargs : `~matplotlib.patches.Patch` properties
    """
    pearson = cov[0, 1] / np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensionl dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0),
                      width=ell_radius_x * 2,
                      height=ell_radius_y * 2,
                      facecolor=facecolor,
                      **kwargs)

    # Calculating the stdandard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = n_std * np.sqrt(cov[0, 0])
    mean_x = mu[0]

    # calculating the stdandard deviation of y ...
    scale_y = n_std * np.sqrt(cov[1, 1])
    mean_y = mu[1]

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)


# Specify data paths
RES_PATH = '../data/alanine_dipeptide/output/'
#TIMESTAMP = '25-08-2021_14-42-16'
TIMESTAMP = '10-09-2021_10-45-29'
FNAME = TIMESTAMP + '_results.h5'

data1 = np.load('../data/alanine_dipeptide/input/alanine-dipeptide-0-250ns-nowater.npy')
data1 = np.einsum('dt->td', data1)
data2 = np.load('../data/alanine_dipeptide/input/alanine-dipeptide-1-250ns-nowater.npy')
data2 = np.einsum('dt->td', data2)
data3 = np.load('../data/alanine_dipeptide/input/alanine-dipeptide-2-250ns-nowater.npy')
data3 = np.einsum('dt->td', data3)
seq2stack = [data1, data2, data3]
data_x = np.stack(seq2stack)
data_x += np.pi

N = data_x.shape[0]
T = data_x.shape[2]

# Plot the raw data
def plot_raw():
    N = data_x.shape[1]
    T = data_x.shape[3]
    c = ['r', 'g', 'b']
    for t in range(T):
        plt.scatter(data_x[0, 2, 0, t], data_x[0, 2, 1, t], c=c[int(data_z[0, 2, t])], alpha=.4)
    plt.xlabel("$\phi$")
    plt.ylabel("$\psi$")
    plt.savefig('../plots/3state_vonMises_hmm/raw_data_scatter.pdf', format='pdf')

# Plot one reconstruction of the latent sequence together with the ground truth
def plot_q_z():
    h5file = tables.open_file(RES_PATH + FNAME)
    q_z_final = np.array(h5file.root.q_z.run_0[f"iteration_4999"])
    h5file.close()
    fig, ax = plt.subplots()

    ax = sns.heatmap(q_z_final[0][:, :10000], cmap='Blues', cbar=False) #.matshow(q_z_final[1], aspect='auto')

    ax.set_xlabel('Time point')
    ax.set_ylabel('Latent state')
    ax.set_ylim([0, 10])
    ax.tick_params(axis='x', top=False, labelbottom=True, labeltop=False)
    #ax.set_xticks([0, 200, 400, 600, 800, 1000])
    #ax.set_xticklabels([0, 200, 400, 600, 800, 1000])
    plt.savefig(f'../plots/alanine_dipeptide/q_z_reconstruction_{TIMESTAMP}.pdf', format='pdf')
    plt.close()

# Plot the `fill states' of the latent modes
def plot_n_modes():
    h5file = tables.open_file(RES_PATH + FNAME)
    n_modes = np.array(h5file.root.modes_vs_iterations)
    h5file.close()
    n_iter = n_modes.shape[-1]
    modes_mean = np.mean(n_modes, axis=0)
    modes_std = np.std(n_modes, axis=0)
    plt.plot(np.arange(1, n_iter + 1), modes_mean)
    plt.fill_between(np.arange(1, n_iter + 1), modes_mean - modes_std, modes_mean + modes_std, alpha=.3)
    plt.xlabel('Iteration')
    plt.ylabel('Number of relevant latent states')
    plt.savefig('../plots/3state_vonMises_hmm/modes_vs_iter_mean_NIW_' + TIMESTAMP + '.pdf', format='pdf')

def print_transition_probabilities():
    h5file = tables.open_file(RES_PATH + FNAME)
    alpha_transition = np.array(h5file.root.concentration_transition.run_0['iteration_499'])
    h5file.close()

    print(np.around(alpha_transition/np.sum(alpha_transition, axis=1)[:, None], 2))

def plot_posterior_NIW():

    h5file = tables.open_file(RES_PATH + FNAME)

    ELBO, run_lengths = [], []
    for run_id, run in enumerate(h5file.root.ELBO):
        ELBO.append(np.array(run))
        ELBO_final = [L[-1] for L in ELBO]
        run_lengths.append(len(np.array(run)))
    run_max_L = np.argmax(ELBO_final)
    run_length = run_lengths[run_max_L] - 1

    mu = np.array(h5file.root.niw_mu0[f"run_{run_max_L}"][f'iteration_{run_length}'])
    nu = np.array(h5file.root.iw_dof[f"run_{run_max_L}"][f'iteration_{run_length}'])
    lambd = np.array(h5file.root.niw_lambda[f"run_{run_max_L}"][f'iteration_{run_length}'])
    psi = np.array(h5file.root.iw_scale[f"run_{run_max_L}"][f'iteration_{run_length}'])
    q_z_final = np.array(h5file.root.q_z[f"run_{run_max_L}"][f'iteration_{run_length}'])
    alpha = np.array(h5file.root.concentration_transition[f"run_{run_max_L}"][f'iteration_{run_length}'])
    h5file.close()

    import matplotlib.colors as colors
    fig, ax = plt.subplots(figsize=(5, 4))
    c = ['tab:blue', 'tab:red', 'tab:green', 'tab:orange', 'magenta']
    map_indices = np.argmax(q_z_final, axis=1)

    for i in range(5):
        for j in range(N):
            tmp = data_x[j, :, (map_indices == i)[j]]
            ax.scatter(tmp[:, 0], tmp[:, 1], c=c[i], alpha=.3, s=10)

    q = q_z_final.sum((0, 2))/(q_z_final.shape[0] * q_z_final.shape[2])
    for i in range(10):
        # Reconstructions
        if q[i] > 0:
            ax.scatter(mu[i, 0], mu[i, 1], marker='d', c=c[i], edgecolor='k', s=80, alpha=1)
            confidence_ellipse(psi[i]/(nu[i] - 3), mu[i], ax, n_std=1, edgecolor='k', alpha=1, lw=2.5)

    plt.xlim((-0.1 * np.pi, 2.1 * np.pi))
    plt.ylim((-0.1 * np.pi, 2.1 * np.pi))
    plt.xticks([0, np.pi, 2 * np.pi],
               ['-$\pi$', '0', '$\pi$'])
    plt.yticks([0, np.pi, 2 * np.pi],
               ['-$\pi$', '0', '$\pi$'])
    plt.xlabel('$\phi$', fontsize='x-large')
    plt.ylabel('$\psi$', fontsize='x-large')
    ax.yaxis.set_label_coords(-0.04, 0.49)
    ax.xaxis.set_label_coords(0.51, -0.06)

    #Add inset
    ax2 = fig.add_axes([0.76, 0.26, 0.21, 0.2])
    q = q_z_final.mean((0, 2))
    for i in range(5):
        ax2.bar(i, q[i], color=c[i])
    ax2.xaxis.set_visible(False)
    ax2.set_yticks([])
    ax2.set_yticklabels([])
    ax2.annotate('', xy=(3, 0.1), xytext=(3, 0.4),
                 arrowprops={'arrowstyle': '-|>', 'lw': 2, 'color': 'tab:orange'},
                 va='center')#, transform=ax2.transData)


    # Annotate structures
    ax.annotate('$\\alpha\'$', xy=mu[0] - np.array([0.6, -0.4]),
                transform=ax.transData, color='k', fontsize=19)
    ax.annotate('$\\beta$', xy=mu[1] - np.array([0., 0.8]),
                transform=ax.transData, color='k', fontsize=19)
    ax.annotate('$\\alpha_L$', xy=mu[2] + np.array([0.2, 0.3]),
                transform=ax.transData, color='k', fontsize=19)
    ax.annotate('$\\alpha_D$', xy=mu[3] + np.array([-1.0, -0.1]),
                transform=ax.transData, color='k', fontsize=19)
    ax.annotate('$\\alpha_R$', xy=mu[4] + np.array([-0, -0.7]),
                transform=ax.transData, color='k', fontsize=19)


    plt.subplots_adjust(left=0.07, right=0.99, top=0.99, bottom=0.1)
    plt.savefig(f'../plots/alanine_dipeptide/posterior_means_{TIMESTAMP}.png', format='png', dpi=300)
    plt.close()

def plot_mean_change():

    h5file = tables.open_file(RES_PATH + FNAME)
    mu1 = np.array(h5file.root.niw_mu0.run_0.iteration_0)
    mu2 = np.array(h5file.root.niw_mu0.run_0.iteration_999)
    mu3 = np.array(h5file.root.niw_mu0.run_0.iteration_1999)
    mu4 = np.array(h5file.root.niw_mu0.run_0.iteration_2999)
    mu5 = np.array(h5file.root.niw_mu0.run_0.iteration_3999)
    mu6 = np.array(h5file.root.niw_mu0.run_0.iteration_4999)
    h5file.close()
    seq2stack = [mu1, mu2, mu3, mu4, mu5, mu6]
    mu = np.stack(seq2stack)

    c = ['r', 'g', 'b', 'y', 'c', 'r', 'g', 'b', 'y', 'c']
    for j, m in enumerate(mu):
        for i, mean in enumerate(m):
            if i > 1:
                plt.scatter(mean[0], mean[1], c=c[i], alpha=(j+1)/len(mu))

    plt.tight_layout()
    plt.savefig(f'../plots/alanine_dipeptide/mean_change_{TIMESTAMP}.pdf', format='pdf')
    plt.close()

def plot_posterior_NIW_lumped():
    """
    Plot the lumped quantities (posterior means/covs and q_z reconstruction) for the 2-ary partition
     of the posterior mean transition matrix (i.e., with 3 states).
    """
    import src.markovchain as mc

    h5file = tables.open_file(RES_PATH + FNAME)
    q_z_final = np.array(h5file.root.q_z.run_0.iteration_1999)
    alpha = np.array(h5file.root.concentration_transition.run_0.iteration_1999)
    mu = np.array(h5file.root.niw_mu0.run_0.iteration_1999)
    h5file.close()

    P = alpha/np.sum(alpha, axis=1)[:, None]
    P = P[:, :-1]

    MC = mc.MarkovChain(P)
    MC.m_partition()

    # Iterate over all partitions
    q_z_lumped = np.zeros((len(MC.KLDR_m), 3, 10, 250000))
    mu_lumped = np.zeros((len(MC.KLDR_m), 10, 2))
    #likelihood = np.zeros(len(MC.KLDR_m))
    for k, phi in enumerate(MC.phi_m):
        for i, elem in enumerate(phi): # that's the 3-ary partition
            q_z_lumped[k, :, elem, :] += q_z_final[:, i, :]
            mu_lumped[k, elem] += np.sum(q_z_final[:, i, :]) * mu[i]

        mu_lumped[k] /= np.sum(q_z_lumped[k], axis=(0, 2))[:, None]

        #data_x_reshaped = np.einsum('ndt->ntd', data_x[0])
        #data_x_reshaped = data_x_reshaped.reshape(-1, 2)
        #for mode_id, mean in enumerate(mu_lumped[k][:k+2]):  # +2 because after that, the values are NaNs (because I divided by zero in the line above)
        #    likelihood[k] += np.sum(q_z_lumped[k, :, mode_id, :].flatten() * stats.multivariate_normal(mean).pdf(data_x_reshaped))

    # We *know* that the optimal aggregation is given by MC.phi_m[1], i.e. a partition into three states
    fig, ax = plt.subplots()

    # renormalize the lumped q's, NOTE: that's only necessary because of some matplotlib weirdness
    q_z_lumped_3states = q_z_lumped[1]/np.sum(q_z_lumped[1], axis=1)[:, None, :]
    q_z_lumped_3states = q_z_lumped_3states[:, :3, :]
    mu_lumped_3states = mu_lumped[1][:3]

    rgba = np.zeros((int(T), 4))
    rgba[:, 0] = q_z_lumped_3states[0, 2, :].flatten()
    rgba[:, 1] = q_z_lumped_3states[0, 1, :].flatten()
    rgba[:, 2] = q_z_lumped_3states[0, 0, :].flatten()
    rgba[:, 3] = 0.2
    ax.scatter(data_x[0, 0, :], data_x[0, 1, :], c=rgba)

    # recompute the lumped covariances; done as in VarDist.update_NIW()

    hpar_nu = np.repeat(float(2 + 1), 3)  # IW-scale
    hpar_psi = np.stack([np.eye(2) for l in range(3)])
    hpar_lambd = np.repeat(float(2), 3)  # MVN-scale
    hpar_mu = np.stack([[np.pi, np.pi] for l in range(3)])

    # Compute the sum of the mode-specific marginals
    Q = np.sum(q_z_lumped_3states, axis=(0, 2))

    # Compute mode-specific data means
    # first, wrap data around three lumped means
    distances = data_x[:, None, :, :] - mu_lumped_3states[None, :, :, None]
    wrapped = data_x[:, None, :, :] - (np.abs(distances) > np.pi) * np.sign(distances) * 2 * np.pi * (
            1 + np.floor(np.abs((distances - np.sign(distances) * np.pi) / (2 * np.pi))))
    data_wrapped = wrapped
    x_weighted = data_wrapped * q_z_lumped_3states[:, :, None, :]
    x_bar = np.mean(x_weighted, axis=(0, 3))

    # Compute posterior mean vectors mu_tilde
    den = hpar_lambd + Q
    num = hpar_lambd[:, None] * hpar_mu + N * T * x_bar
    mu_tilde = num / den[:, None]
    mu_tilde[mu_tilde > 2 * np.pi] -= 2 * np.pi
    mu_tilde[mu_tilde < 0] += 2 * np.pi
    mu = mu_tilde

    # Compute posterior psi_tilde
    V = (hpar_lambd * (Q - N * T) / den)[:, None, None] * np.einsum('ld,le->lde', hpar_mu, hpar_mu)
    M = ((hpar_lambd * T * N) / (hpar_lambd + Q))[:, None, None] * np.einsum('ld,le->lde',
                                                                                                 x_bar - hpar_mu,
                                                                                                 x_bar - hpar_mu)

    xxT = np.einsum('nldt,nlet->nldet', data_wrapped, data_wrapped)
    xxT_weighted = np.sum(np.einsum('nldet,nlt->nldet', xxT, q_z_lumped_3states), axis=(0, 4))
    xbarxbarT = ((hpar_lambd + N * T) / (hpar_lambd + Q))[:, None, None] * np.einsum('ld,le->lde', x_bar, x_bar)
    S = xxT_weighted - N * T * xbarxbarT

    # Compute and update posterior scale/dof-parameters
    nu = hpar_nu + Q
    lambd = hpar_lambd + Q
    psi = hpar_psi + M + V + S

    for l in range(3):
        #ax.scatter(mu_lumped[1][l, 0], mu_lumped[1][l, 1], marker='d', c='w', s=140)
        # Reconstructions
        ax.scatter(mu[l, 0], mu[l, 1], marker='d', c='w', s=140, alpha=.9)
        try:
            confidence_ellipse(psi[l] / (nu[l] - 3), mu[l], ax, n_std=1, edgecolor='white', alpha=.8, lw=3)
        except:
            pass

    plt.xlim((-0.1 * np.pi, 2.1 * np.pi))
    plt.ylim((-0.1 * np.pi, 2.1 * np.pi))
    plt.xticks([0, np.pi/2, np.pi, 3/2 * np.pi, 2 * np.pi],
               ['-$\pi$', '-$\pi/2$', '0', '$\pi/2$', '$\pi$'])
    plt.yticks([0, np.pi/2, np.pi, 3/2 * np.pi, 2 * np.pi],
               ['-$\pi$', '-$\pi/2$', '0', '$\pi/2$', '$\pi$'])
    #fig, ax = plt.subplots(3, sharex=True)
    #ax[0].plot(data_x[0, 0, 0, 3000:5000], lw=.8, alpha=.8, label='x')
    #ax[0].plot(data_x[0, 0, 1, 3000:5000], lw=.8, alpha=.8, label='y')
    ##ax[1].matshow(q_z_final[0, :, 3000:5000], aspect='auto')
    #ax[1] = sns.heatmap(q_z_final[0, :, 3000:5000], ax=ax[1], cmap='Blues', cbar=False)
    #ax[2] = sns.heatmap(q_z_lumped[1, 0, :3, 3000:5000], ax=ax[2], cmap='Blues', cbar=False)

    ##ax[1].set_xlabel('Time point')
    #ax[1].set_ylabel('Latent state')
    ##ax[1].tick_params(axis='x', top=False, labelbottom=True, labeltop=False)
    ##ax[1].set_xticks([0, 1000, 2000])# 6000, 8000, 10000])
    ##ax[1].set_xticklabels([0, 1000, 2000])#, 6000, 8000, 10000])
    #ax[1].set_ylim([0, 10])

    #ax[2].set_xticks([0, 500, 1000, 1500, 2000])
    #ax[2].set_xticklabels([0, 500, 1000, 1500, 2000])
    #ax[2].set_xlabel('Time point')
    #ax[2].set_ylabel('Aggregated\nlatent state')
    #ax[2].set_ylim([0, 3])
    #plt.xticks(rotation=0)

    ## Draw vertical dashed lines at change points
    #MAP = np.argmax(q_z_lumped[1, 0, :3, 3000:5000], axis=0)
    #change_points = np.where(np.diff(MAP) != 0)[0]
    #for change_point in change_points:
    #    ax[0].axvline(change_point, c='r', ls='--', lw=.5)
    #    ax[1].axvline(change_point, c='r', ls='--', lw=.5)
    #    ax[2].axvline(change_point, c='r', ls='--', lw=.5)

    plt.xlabel('$\phi$')
    plt.ylabel('$\psi$')
    plt.tight_layout()
    plt.subplots_adjust(left=0.115, right=0.99, top=0.99, bottom=0.12)
    plt.savefig(f'../plots/alanine_dipeptide/posterior_means_lumped_reconstruction_{TIMESTAMP}.pdf', format='pdf')
    plt.close()

def plot_q_z_lumped():
    import src.markovchain as mc

    h5file = tables.open_file(RES_PATH + FNAME)
    q_z_final = np.array(h5file.root.q_z.run_0.iteration_1999)
    alpha = np.array(h5file.root.concentration_transition.run_0.iteration_1999)
    mu = np.array(h5file.root.niw_mu0.run_0.iteration_1999)
    h5file.close()

    P = alpha/np.sum(alpha, axis=1)[:, None]
    P = P[:, :-1]

    MC = mc.MarkovChain(P)
    MC.m_partition()

    # Iterate over all partitions
    q_z_lumped = np.zeros((len(MC.KLDR_m), 3, 10, 250000))
    mu_lumped = np.zeros((len(MC.KLDR_m), 10, 2))
    #likelihood = np.zeros(len(MC.KLDR_m))
    for k, phi in enumerate(MC.phi_m):
        for i, elem in enumerate(phi): # that's the 3-ary partition
            q_z_lumped[k, :, elem, :] += q_z_final[:, i, :]
            mu_lumped[k, elem] += np.sum(q_z_final[:, i, :]) * mu[i]

        mu_lumped[k] /= np.sum(q_z_lumped[k], axis=(0, 2))[:, None]

        #data_x_reshaped = np.einsum('ndt->ntd', data_x[0])
        #data_x_reshaped = data_x_reshaped.reshape(-1, 2)
        #for mode_id, mean in enumerate(mu_lumped[k][:k+2]):  # +2 because after that, the values are NaNs (because I divided by zero in the line above)
        #    likelihood[k] += np.sum(q_z_lumped[k, :, mode_id, :].flatten() * stats.multivariate_normal(mean).pdf(data_x_reshaped))

    # renormalize the lumped q's, NOTE: that's only necessary because of some matplotlib weirdness
    q_z_lumped_3states = q_z_lumped[1]/np.sum(q_z_lumped[1], axis=1)[:, None, :]
    q_z_lumped_3states = q_z_lumped_3states[:, :3, :]

    # Plot part of the trajectory, the VI output reconstruction of q_z and the lumping of q_z
    fig, ax = plt.subplots(3, sharex=True)
    ax[0].plot(data_x[0, 0, 15000:25000], lw=.8, alpha=.8, label='x')
    ax[0].plot(data_x[0, 1, 15000:25000], lw=.8, alpha=.8, label='y')
    ax[0].set_ylim([-0.1*np.pi, 2.1*np.pi])
    ax[0].set_yticks([0, np.pi, 2*np.pi])
    ax[0].set_yticklabels(['$-\pi$', '0', '$\pi$'])
    #ax[0].set_ylabel('Trajectory\ncoordinates')
    ax[0].set_ylabel('$\phi, \psi$', labelpad=-8)
    # ax[1].matshow(q_z_final[0, :, 3000:5000], aspect='auto')
    #ax[1] = sns.heatmap(q_z_final[0, :, 15000:25000], ax=ax[1], cmap='Blues', cbar=False)
    ax[1].matshow(q_z_final[0, :, 15000:25000], aspect='auto', cmap='Blues')
    #ax[2] = sns.heatmap(q_z_lumped_3states[0, :, 15000:25000], ax=ax[2], cmap='Blues', cbar=False)
    ax[2].matshow(q_z_lumped_3states[0, :, 15000:25000], aspect='auto', cmap='Blues')

    ## ax[1].set_xlabel('Time point')
    #ax[1].set_ylabel('Latent state')
    ax[1].set_ylabel('$z$')
    ax[1].tick_params(axis='x', top=False, labelbottom=False, labeltop=False)
    ## ax[1].set_xticks([0, 1000, 2000])# 6000, 8000, 10000])
    ## ax[1].set_xticklabels([0, 1000, 2000])#, 6000, 8000, 10000])
    ax[1].set_yticks([0, 2, 4, 6, 8])
    ax[1].set_yticklabels([1, 3, 5, 7, 9])
    ax[1].set_ylim([9.5, -0.5])

    ax[2].set_xticks([0, 2500, 5000, 7500, 10000])
    ax[2].set_xticklabels([0, 2500, 5000, 7500, 10000])
    ax[2].set_xlabel('Time $[ps]$')
    #ax[2].set_ylabel('Aggregated\nlatent state')
    ax[2].set_ylabel('$z_{agg}$')
    ax[2].set_yticks([0, 1, 2])
    ax[2].set_yticklabels([1, 2, 3])
    ax[2].set_ylim([2.5, -0.5])
    ax[2].tick_params(axis='x', top=False, labelbottom=True, labeltop=False)
    plt.xticks(rotation=0)

    ## Draw vertical dashed lines at change points
    #MAP = np.argmax(q_z_lumped[1, 0, :3, 3000:5000], axis=0)
    #change_points = np.where(np.diff(MAP) != 0)[0]
    #for change_point in change_points:
    #    ax[0].axvline(change_point, c='r', ls='--', lw=.5)
    #    ax[1].axvline(change_point, c='r', ls='--', lw=.5)
    #    ax[2].axvline(change_point, c='r', ls='--', lw=.5)

    plt.tight_layout()
    plt.subplots_adjust(left=0.08, right=0.95, top=0.99, bottom=0.12, hspace=0.15)
    plt.savefig(f'../plots/alanine_dipeptide/q_z_lumped_reconstruction_{TIMESTAMP}.pdf', format='pdf')
    plt.close()

def plot_ELBO():
    """
    Plot the ELBO trajectories of all simulations.
    """

    h5file = tables.open_file(RES_PATH + FNAME, 'a')
    ELBO = []
    for run_id, run in enumerate(h5file.root.ELBO):
        print(run_id)
        ELBO.append(np.array(run.iteration_119))
    h5file.close()

    for trajectory in ELBO:
        plt.plot(trajectory[5:])

    plt.show()
    pass

#plot_ELBO()
#plot_q_z_lumped()
plot_posterior_NIW()
#plot_posterior_NIW_lumped()
#plot_mean_change()

