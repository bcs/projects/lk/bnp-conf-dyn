"""
Plot all figures pertaining to the 3-state n_seq-HMM with NIW prior.
"""
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import tables
from matplotlib.patches import Ellipse
import matplotlib.transforms as transforms

matplotlib.rcParams['axes.linewidth'] = 1.5  # set the value globally
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['mathtext.rm'] = 'serif'


def confidence_ellipse(cov, mu, ax, n_std=3.0, facecolor='none', **kwargs):
    """
    Create a plot of the covariance confidence ellipse of *x* and *y*.

    Parameters
    ----------
    cov : array-like, shape (2, 2)
        Covariance matrix.

    ax : matplotlib.axes.Axes
        The axes object to draw the ellipse into.

    n_std : float
        The number of standard deviations to determine the ellipse's radiuses.

    Returns
    -------
    matplotlib.patches.Ellipse

    Other parameters
    ----------------
    kwargs : `~matplotlib.patches.Patch` properties
    """
    pearson = cov[0, 1] / np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensionl dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0),
                      width=ell_radius_x * 2,
                      height=ell_radius_y * 2,
                      facecolor=facecolor,
                      **kwargs)

    # NOTE to self: in the following lines, I double-checked that this procedure indeed gives the correct result
    # via the more conventional computation of eigenvalues/-vectors
    # import numpy.linalg as la
    # w, v = la.eig(cov)
    # ellipse1 = Ellipse((0, 0), width=n_std * np.sqrt(w[0])*2, height=n_std * np.sqrt(w[1])*2, facecolor='yellow', **kwargs)
    # theta = np.arccos(v[0, 0]/np.sqrt(np.sum(v[:, 0]**2)))
    # transf = transforms.Affine2D().rotate(-theta).translate(niw_mu0[0], niw_mu0[1])
    # ellipse1.set_transform(transf + ax.transData)
    # ax.add_patch(ellipse1)

    # Calculating the stdandard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = n_std * np.sqrt(cov[0, 0])
    mean_x = mu[0]

    # calculating the stdandard deviation of y ...
    scale_y = n_std * np.sqrt(cov[1, 1])
    mean_y = mu[1]

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)


# Specify data paths
RES_PATH = '../data/ionchannel/output/'
# TIMESTAMP = '14-06-2021_16-07-49'
TIMESTAMP = '25-08-2021_16-55-23'
FNAME = TIMESTAMP + '_results.h5'

data = np.genfromtxt('../data/ionchannel/input/+160mV_1min.asc')[90000:240000]
N_datasets = 1
T = data.shape[0]


# Plot the raw data
def plot_raw():
    data_full = np.genfromtxt('../data/ionchannel/input/+160mV_1min.asc')
    time = np.arange(data_full.shape[0])

    fig, ax = plt.subplots(2, figsize=(10, 6), gridspec_kw={'height_ratios':[1, 1], 'hspace':0.25})
    ax[0].plot(time, data_full, lw=.8)

    ax[0].set_xlabel('Time [s]', fontdict={'size': 'x-large', 'name': 'cmr10'})
    ax[0].set_xticks([0, 50000, 100000, 150000, 200000, 250000, 300000])
    ax[0].set_xticklabels([0, 10, 20, 30, 40, 50, 60])
    ax[0].set_ylabel('$I\,\,\mathrm{[pA]}$', fontsize='x-large')
    ax[0].set_yticks(np.array([0, 0.5, 1]) * 1e-11)
    ax[0].set_yticklabels([0, 5, 10])
    ax[0].set_ylim((-0.3e-11, 1.5e-11))

    #plt.subplots_adjust(left=0.07, right=0.96, top=0.99, bottom=0.15)
    #plt.savefig(f'../plots/ionchannel/raw_data_full.pdf', format='pdf')
    #plt.close()

    #fig, ax = plt.subplots(1, figsize=(10, 3))
    ax[1].plot(time[90000:240000], data_full[90000:240000], lw=.8)

    ax[1].set_xlabel('Time [s]', fontdict={'size': 'x-large', 'name': 'cmr10'})
    ax[1].set_xticks([90000, 140000, 190000, 240000])
    ax[1].set_xticklabels([18, 28, 38, 48])
    ax[1].set_ylabel('$I\,\,\mathrm{[pA]}$', fontsize='x-large')
    ax[1].set_yticks(np.array([0, 0.5, 1]) * 1e-11)
    ax[1].set_yticklabels([0, 5, 10])
    ax[1].set_ylim((-0.3e-11, 1.5e-11))

    # Add box to show which data I used for inference and which I used to visualize
    import matplotlib.patches as patches

    # Patch used for inference
    rect_inf = patches.Rectangle((90000, -0.25e-11), 150000, 1.7e-11, edgecolor='tab:red',
                                 facecolor='none', lw=1.5, zorder=10)
    ax[0].add_patch(rect_inf)

    # Patch used for visualization
    rect_vis = patches.Rectangle((154200, -0.22e-11), 5000, 1.6e-11, edgecolor='tab:orange',
                                 facecolor='none', lw=1.5, zorder=10)
    ax[1].add_patch(rect_vis)

    plt.subplots_adjust(left=0.06, right=0.99, top=0.99, bottom=0.09)
    plt.savefig(f'../plots/ionchannel/raw_data.pdf', format='pdf')
    plt.close()


# Plot one reconstruction of the latent sequence together with the ground truth
def plot_q_z():
    h5file = tables.open_file(RES_PATH + FNAME)

    ELBO, run_lengths = [], []
    for run_id, run in enumerate(h5file.root.ELBO):
        ELBO.append(np.array(run))
        ELBO_final = [L[-1] for L in ELBO]
        run_lengths.append(len(np.array(run)))
    run_max_L = np.argmax(ELBO_final)
    run_length = run_lengths[run_max_L] - 1

    mu = np.array(h5file.root.niw_mu0[f"run_{run_max_L}"][f'iteration_{run_length}'])
    nu = np.array(h5file.root.iw_dof[f"run_{run_max_L}"][f'iteration_{run_length}'])
    psi = np.array(h5file.root.iw_scale[f"run_{run_max_L}"][f'iteration_{run_length}'])
    q_z = np.array(h5file.root.q_z[f"run_{run_max_L}"][f'iteration_{run_length}'])
    h5file.close()

    fig, ax = plt.subplots(2, sharex=True, gridspec_kw={'height_ratios': [4, 1], 'hspace': 0.1}, figsize=(5, 2.5))

    # ax[0].plot(data[64550:75400], lw=.8)
    # ax[1].matshow(q_z[0, :3, 64500:75400], aspect='auto', cmap='Blues')
    # ax[1].tick_params(axis='x', top=False, labelbottom=True, labeltop=False)
    ax[0].plot(data[64200:69200], lw=.8)
    ax[1].matshow(q_z[0, :3, 64200:69200], aspect='auto', cmap='Blues')
    ax[1].tick_params(axis='x', top=False, labelbottom=True, labeltop=False)

    c = ['tab:orange', 'tab:red', 'tab:green']
    for i in range(3):
        ax[0].axhline(mu[i], c=c[i], alpha=.7, ls='--', lw=1.5)
        ax[0].fill_between(np.arange(0, 5000), mu[i] - np.sqrt(psi[i] / (nu[i] - 2)).flatten(),
                           mu[i] + np.sqrt(psi[i] / (nu[i] - 2)).flatten(), color=c[i], alpha=.2)

    # ax[0].set_ylim((-3e-12, 1.5e-11))
    ax[0].set_ylabel('$I\,\,\mathrm{[pA]}$', fontsize='x-large')
    ax[0].set_yticks(np.array([0, 0.5, 1]) * 1e-11)
    ax[0].set_yticklabels([0, 5, 10])
    ax[0].yaxis.set_label_coords(-0.05, 0.5)

    ax[1].set_xlabel('Time [s]', fontdict={'size': 'x-large', 'name': 'cmr10'})
    ax[1].set_ylabel('$Z$', fontsize='x-large')
    ax[1].tick_params(axis='x', top=False, labelbottom=True, labeltop=False)
    ax[1].set_xticks([0, 2500, 5000])
    ax[1].set_xticklabels([0, 0.5, 1])
    ax[1].set_yticks([0, 1, 2])
    ax[1].set_yticklabels([1, 2, 3])
    ax[1].xaxis.set_label_coords(0.5, -0.65)
    ax[1].yaxis.set_label_coords(-0.05, 0.5)
    # ax[1].set_ylim([9.5, -0.5])
    # plt.xticks(rotation=0)

    plt.subplots_adjust(left=0.09, right=0.99, top=0.99, bottom=0.19)
    plt.savefig(f'../plots/ionchannel/q_z_reconstruction_{TIMESTAMP}.pdf', format='pdf')
    plt.close()


# Plot the `fill states' of the latent modes
def plot_n_modes():
    h5file = tables.open_file(RES_PATH + FNAME)
    n_modes = np.array(h5file.root.modes_vs_iterations)
    h5file.close()
    n_iter = n_modes.shape[-1]
    modes_mean = np.mean(n_modes, axis=0)
    # modes_std = np.std(n_modes, axis=0)
    fig, ax = plt.subplots()
    for trace in n_modes:
        ax.plot(np.arange(1, n_iter + 1), trace, lw=.9)
    # plt.fill_between(np.arange(1, n_iter + 1), modes_mean - modes_std, modes_mean + modes_std, alpha=.3)
    ax.plot(np.arange(1, n_iter + 1), modes_mean, c='r', lw=3, alpha=.6)
    plt.xlabel('Iteration')
    plt.ylabel('Relevant latent states')
    plt.ylim((0, 11))
    plt.yticks(range(11), range(11))
    ax.set_aspect(55)
    plt.tight_layout()
    plt.subplots_adjust(left=0.1, right=0.99, top=0.99, bottom=0.0)
    plt.savefig('../plots/3state_N_hmm/modes_vs_iter_mean_NIW_' + TIMESTAMP + '.pdf', format='pdf')
    plt.close()


def print_transition_probabilities():
    h5file = tables.open_file(RES_PATH + FNAME)
    alpha_transition = np.array(h5file.root.concentration_transition.run_0['iteration_499'])
    h5file.close()

    print(np.around(alpha_transition / np.sum(alpha_transition, axis=1)[:, None], 2))


def plot_posterior_NIW():
    h5file = tables.open_file(RES_PATH + FNAME)

    ELBO = []
    for run_id, run in enumerate(h5file.root.ELBO):
        ELBO.append(np.array(run.iteration_99))
        ELBO_final = [L[-1] for L in ELBO]
    run_max_L = np.argmax(ELBO_final)

    mu = np.array(h5file.root.niw_mu0[f"run_{run_max_L}"].iteration_99)
    nu = np.array(h5file.root.iw_dof[f"run_{run_max_L}"].iteration_99)
    lambd = np.array(h5file.root.niw_lambda[f"run_{run_max_L}"].iteration_99)
    psi = np.array(h5file.root.iw_scale[f"run_{run_max_L}"].iteration_99)
    q_z_final = np.array(h5file.root.q_z[f"run_{run_max_L}"].iteration_99)
    h5file.close()

    # Ground-truth values
    mu_true = np.array([[-3, -3],
                        [3, -3],
                        [0, 2]])
    sig_true = np.zeros((3, 2, 2))
    sig_true[0] = np.array([[1, .5],
                            [.5, 1]])
    sig_true[1] = np.array([[1, -.5],
                            [-.5, 1]])
    sig_true[2] = np.eye(2) * np.array([5, 1])

    # Actual plotting
    import matplotlib.colors as colors
    # identify three most `filled' modes (as I know the model result = 3 modes already)
    indices = np.argsort(np.sum(q_z_final, axis=(0, 2)))[-3:]

    rgba_r = colors.to_rgba('r')
    rgba_g = colors.to_rgba('g')
    rgba_b = colors.to_rgba('b')
    rgba_qr = np.zeros((int(N * T), 4))
    rgba_qr[:] = rgba_r
    rgba_qg = np.zeros((int(N * T), 4))
    rgba_qg[:] = rgba_g
    rgba_qb = np.zeros((int(N * T), 4))
    rgba_qb[:] = rgba_b

    rgba_qr[:, 3] = q_z_final[:, indices[0], :].flatten()
    rgba_qg[:, 3] = q_z_final[:, indices[1], :].flatten()
    rgba_qb[:, 3] = q_z_final[:, indices[2], :].flatten()

    fig, ax = plt.subplots()
    rgba = np.zeros((int(N * T), 4))
    rgba[:, 0] = q_z_final[:, indices[2], :].flatten()
    rgba[:, 1] = q_z_final[:, indices[1], :].flatten()
    rgba[:, 2] = q_z_final[:, indices[0], :].flatten()
    rgba[:, 3] = 0.2
    ax.scatter(data_x[run_max_L, :, 0, :], data_x[run_max_L, :, 1, :], c=rgba)
    for i in indices:
        # Reconstructions
        ax.scatter(mu[i, 0], mu[i, 1], marker='n_dim', c='w', s=140, alpha=.8)
        confidence_ellipse(psi[i] / (nu[i] - 3), mu[i], ax, n_std=1, edgecolor='white', alpha=.8, lw=3)
    for i in range(3):
        # Ground truth
        ax.scatter(mu_true[i, 0], mu_true[i, 1], marker='x', c='k', s=110, alpha=1)
        confidence_ellipse(sig_true[i], mu_true[i], ax, n_std=1, edgecolor='black', lw=2, alpha=1)

    plt.xlim((-10, 10))
    plt.ylim((-9, 9))
    plt.xlabel('$x_1$')
    plt.ylabel('$x_2$')
    plt.tight_layout()
    plt.subplots_adjust(left=0.1, right=0.96, top=0.99, bottom=0.12)
    plt.savefig(f'../plots/3state_N_hmm/posterior_means_{TIMESTAMP}.pdf', format='pdf')
    plt.close()


def plot_beta():
    """
    Plot the stick-breaking measure at the end of the simulation.
    """
    h5file = tables.open_file(RES_PATH + FNAME)
    beta = np.array(h5file.root.top_level_gem.run_0[f"iteration_999"])
    h5file.close()

    plt.bar(range(1, 12), beta)
    plt.show()
    plt.close()


def plot_ELBO():
    """
    Plot the ELBO trajectories of all simulations.
    """

    h5file = tables.open_file(RES_PATH + FNAME, 'a')
    ELBO = []
    for run_id, run in enumerate(h5file.root.ELBO):
        print(run_id)
        ELBO.append(np.array(run.iteration_99))
    h5file.close()

    for trajectory in ELBO:
        plt.plot(trajectory[2:])

    plt.show()
    pass


# plot_ELBO()
plot_q_z()
# plot_n_modes()
# plot_posterior_NIW()
#plot_raw()
# plot_beta()
