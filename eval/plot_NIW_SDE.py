"""
Plot all figures pertaining to SDE dynamics dataset.
"""
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import tables
from matplotlib.patches import Ellipse
import matplotlib.transforms as transforms
import scipy.stats as stats

matplotlib.rcParams['axes.linewidth'] = 1.5 # set the value globally
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['mathtext.rm'] = 'serif'


def confidence_ellipse(cov, mu, ax, n_std=3.0, facecolor='none', **kwargs):
    """
    Create a plot of the covariance confidence ellipse of *x* and *y*.

    Parameters
    ----------
    cov : array-like, shape (2, 2)
        Covariance matrix.

    ax : matplotlib.axes.Axes
        The axes object to draw the ellipse into.

    n_std : float
        The number of standard deviations to determine the ellipse's radiuses.

    Returns
    -------
    matplotlib.patches.Ellipse

    Other parameters
    ----------------
    kwargs : `~matplotlib.patches.Patch` properties
    """
    pearson = cov[0, 1] / np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensionl dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0),
                      width=ell_radius_x * 2,
                      height=ell_radius_y * 2,
                      facecolor=facecolor,
                      **kwargs)

    # Calculating the stdandard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = n_std * np.sqrt(cov[0, 0])
    mean_x = mu[0]

    # calculating the stdandard deviation of y ...
    scale_y = n_std * np.sqrt(cov[1, 1])
    mean_y = mu[1]

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)


################################################################################
################################################################################

def U(x, y):
    """
    Potential as defined in Wu et al.

    Parameter
    ---------
    x, y : float
        2D-position
    """
    return (3 * np.exp(-x**2 - (y - 1/3)**2) - 3 * np.exp(-x**2 - (y - 5/3)**2) - 5 * np.exp(-(x-1)**2 - y**2)
            - 5 * np.exp(-(x+1)**2 - y**2) + 1/5 * x**4 + 1/5 * (y - 1/3)**4)


# Specify data paths
RES_PATH = '../data/3well_SDE/output/'
#TIMESTAMP = '24-08-2021_18-06-10'
TIMESTAMP = '10-09-2021_10-37-28'
FNAME = TIMESTAMP + '_results.h5'

data_x = np.load('../data/3well_SDE/input/x_3_10000.npy')

# Plot energy landscape
def plot_energy_landscape():
    x = np.linspace(-2, 2, 200)
    y = np.linspace(-1, 2, 200)
    X, Y = np.meshgrid(x, y)

    plt.contourf(X, Y, U(X, Y), levels=50)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.xticks([-2, -1, 0, 1, 2])
    plt.yticks([-1, 0, 1, 2])
    plt.savefig('../plots/3well_SDE/energy_landscape.pdf', format='pdf')
    plt.close()


# Plot one reconstruction of the latent sequence together with the ground truth
def plot_q_z():
    h5file = tables.open_file(RES_PATH + FNAME)

    ELBO, run_lengths = [], []
    for run_id, run in enumerate(h5file.root.ELBO):
        ELBO.append(np.array(run))
        ELBO_final = [L[-1] for L in ELBO]
        run_lengths.append(len(np.array(run)))
    run_max_L = np.argmax(ELBO_final)
    run_length = run_lengths[run_max_L] - 1

    q_z_final = np.array(h5file.root.q_z[f'run_{run_max_L}'][f'iteration_{run_length}'])
    h5file.close()

    fig, ax = plt.subplots(2, sharex=True, gridspec_kw={'height_ratios': [4, 1]}, figsize=(5, 2))
    ax[0].plot(data_x[0, 0, 3000:5000], lw=.8, alpha=.8, label='x', c='k')
    ax[0].plot(data_x[0, 1, 3000:5000], lw=.8, alpha=.8, label='y', c='tab:orange')
    ax[1].matshow(q_z_final[0, :3, 3000:5000], cmap='Blues', aspect='auto')

    ax[0].set_ylabel('$X$', fontsize='x-large', labelpad=-7)
    ax[1].set_ylabel('$Z$', fontsize='x-large')
    ax[1].set_xlabel('Time', fontdict={'size': 'x-large', 'name': 'cmr10'})
    ax[1].tick_params(axis='x', top=False, labelbottom=True, labeltop=False)
    ax[1].set_xticks([0, 1000, 2000])# 6000, 8000, 10000])
    ax[1].set_xticklabels([0, 1000, 2000])#, 6000, 8000, 10000])
    ax[1].set_yticks([0, 1, 2])
    ax[1].set_yticklabels([1, '', 3])
    #ax[1].set_ylim([9.5, -0.5])

    plt.xticks(rotation=0)

    plt.subplots_adjust(left=0.09, right=0.96, top=0.99, bottom=0.22, hspace=0.12)
    plt.savefig(f'../plots/3well_SDE/q_z_reconstruction_{TIMESTAMP}.pdf', format='pdf')
    plt.close()


# Plot the `fill states' of the latent modes
def plot_n_modes():
    h5file = tables.open_file(RES_PATH + FNAME)
    n_modes = np.array(h5file.root.modes_vs_iterations)
    h5file.close()
    n_iter = n_modes.shape[-1]
    modes_mean = np.mean(n_modes, axis=0)
    modes_std = np.std(n_modes, axis=0)
    plt.plot(np.arange(1, n_iter + 1), modes_mean)
    plt.fill_between(np.arange(1, n_iter + 1), modes_mean - modes_std, modes_mean + modes_std, alpha=.3)
    plt.xlabel('Iteration')
    plt.ylabel('Number of relevant latent states')
    plt.savefig('../plots/3well_SDE/modes_vs_iter_mean_NIW_' + TIMESTAMP + '.pdf', format='pdf')
    plt.close()


def print_transition_probabilities():
    h5file = tables.open_file(RES_PATH + FNAME)
    alpha_transition = np.array(h5file.root.concentration_transition.run_0['iteration_499'])
    h5file.close()

    print(np.around(alpha_transition/np.sum(alpha_transition, axis=1)[:, None], 2))


def plot_posterior_NIW():

    h5file = tables.open_file(RES_PATH + FNAME)

    ELBO, run_lengths = [], []
    for run_id, run in enumerate(h5file.root.ELBO):
        ELBO.append(np.array(run))
        ELBO_final = [L[-1] for L in ELBO]
        run_lengths.append(len(np.array(run)))
    run_max_L = np.argmax(ELBO_final)
    run_length = run_lengths[run_max_L] - 1

    mu = np.array(h5file.root.niw_mu0[f"run_{run_max_L}"][f'iteration_{run_length}'])
    nu = np.array(h5file.root.iw_dof[f"run_{run_max_L}"][f'iteration_{run_length}'])
    lambd = np.array(h5file.root.niw_lambda[f"run_{run_max_L}"][f'iteration_{run_length}'])
    psi = np.array(h5file.root.iw_scale[f"run_{run_max_L}"][f'iteration_{run_length}'])
    q_z_final = np.array(h5file.root.q_z[f"run_{run_max_L}"][f'iteration_{run_length}'])
    alpha = np.array(h5file.root.concentration_transition[f"run_{run_max_L}"][f'iteration_{run_length}'])
    h5file.close()

    x = np.linspace(-2, 2, 200)
    y = np.linspace(-1, 2, 200)
    X, Y = np.meshgrid(x, y)

    fig, ax = plt.subplots(figsize=(5, 4))

    ax.contourf(X, Y, U(X, Y), levels=50, cmap='gray')
    ax.set_xlabel('$X_1$', fontsize='x-large')
    ax.set_ylabel('$X_2$', fontsize='x-large')
    plt.xticks([-2, -1, 0, 1, 2])
    plt.yticks([-1, 0, 1, 2])

    #mu_true = np.array([[-1, 0],
    #                    [1, 0],
    #                    [0, 1.5]])

    c = ['tab:red', 'tab:blue', 'tab:green']
    for i in range(3):
        # Reconstructions
        ax.scatter(mu[i, 0], mu[i, 1], marker='d', c=c[i], s=120, alpha=.7)
        confidence_ellipse(psi[i]/(nu[i] - 3), mu[i], ax, n_std=1, edgecolor=c[i], alpha=.7, lw=2)
    plt.xlim((-2, 2))
    plt.ylim((-1, 2))
    ax.yaxis.set_label_coords(-0.06, 0.5)

    #Add inset
    ax2 = fig.add_axes([0.76, 0.76, 0.21, 0.2])
    q = q_z_final.mean((0, 2))
    for i in range(3):
        ax2.bar(i, q[i], color=c[i])
    ax2.xaxis.set_visible(False)
    ax2.set_yticks([])
    ax2.set_yticklabels([])
    #ax2.annotate('', xy=(3, 0.1), xytext=(3, 0.4),
    #             arrowprops={'arrowstyle': '-|>', 'lw': 2, 'color': 'tab:orange'},
    #             va='center')#, transform=ax2.transData)

    plt.tight_layout()
    plt.subplots_adjust(left=0.1, right=0.98, top=0.98, bottom=0.12)
    plt.savefig(f'../plots/3well_SDE/posterior_means_{TIMESTAMP}.pdf', format='pdf')
    plt.close()


def plot_KLDR():
    """
    Plot the KLDR for all possible m-partitions of the posterior mean transition matrix.
    """
    import src.markovchain as mc

    h5file = tables.open_file(RES_PATH + FNAME)
    q_z_final = np.array(h5file.root.q_z.run_0.iteration_1999)
    alpha = np.array(h5file.root.concentration_transition.run_0.iteration_1999)
    mu = np.array(h5file.root.niw_mu0.run_0.iteration_1999)
    h5file.close()

    P = alpha/np.sum(alpha, axis=1)[:, None]
    P = P[:, :-1]

    MC = mc.MarkovChain(P)
    MC.m_partition()

    # Iterate over all partitions
    q_z_lumped = np.zeros((len(MC.KLDR_m), 3, 10, 10000))
    mu_lumped = np.zeros((len(MC.KLDR_m), 10, 2))
    #likelihood = np.zeros(len(MC.KLDR_m))
    for k, phi in enumerate(MC.phi_m):
        for i, elem in enumerate(phi): # that's the 3-ary partition
            q_z_lumped[k, :, elem, :] += q_z_final[:, i, :]
            mu_lumped[k, elem] += np.sum(q_z_final[:, i, :]) * mu[i]

        mu_lumped[k] /= np.sum(q_z_lumped[k], axis=(0, 2))[:, None]

        #data_x_reshaped = np.einsum('ndt->ntd', data_x[0])
        #data_x_reshaped = data_x_reshaped.reshape(-1, 2)
        #for mode_id, mean in enumerate(mu_lumped[k][:k+2]):  # +2 because after that, the values are NaNs (because I divided by zero in the line above)
        #    likelihood[k] += np.sum(q_z_lumped[k, :, mode_id, :].flatten() * stats.multivariate_normal(mean).pdf(data_x_reshaped))

    # We *know* that the optimal aggregation is given by MC.phi_m[1], i.e. a partition into three states
    fig, ax = plt.subplots(3, sharex=True)
    ax[0].plot(data_x[0, 0, 0, 3000:5000], lw=.8, alpha=.8, label='x')
    ax[0].plot(data_x[0, 0, 1, 3000:5000], lw=.8, alpha=.8, label='y')
    #ax[1].matshow(q_z_final[0, :, 3000:5000], aspect='auto')
    #ax[1] = sns.heatmap(q_z_final[0, :, 3000:5000], ax=ax[1], cmap='Blues', cbar=False)
    #ax[2] = sns.heatmap(q_z_lumped[1, 0, :3, 3000:5000], ax=ax[2], cmap='Blues', cbar=False)
    ax[1].matshow(q_z_final[0, :, 3000:5000], cmap='Blues', aspect='auto')
    ax[2].matshow(q_z_lumped[1, 0, :, 3000:5000], cmap='Blues', aspect='auto')

    #ax[0].set_ylabel('Trajectory\ncoordinates')
    ax[0].set_ylabel('$x, y$', labelpad=-7)
    #ax[1].set_xlabel('Time point')
    #ax[1].set_ylabel('Latent state')
    ax[1].set_ylabel('$z$')
    #ax[1].tick_params(axis='x', top=False, labelbottom=True, labeltop=False)
    #ax[1].set_xticks([0, 1000, 2000])# 6000, 8000, 10000])
    #ax[1].set_xticklabels([0, 1000, 2000])#, 6000, 8000, 10000])
    ax[1].set_yticks([0, 2, 4, 6, 8])
    ax[1].set_yticklabels([1, 3, 5, 7, 9])
    ax[1].set_ylim([9.5, -0.5])

    ax[2].set_xticks([0, 500, 1000, 1500, 2000])
    ax[2].set_xticklabels([0, 500, 1000, 1500, 2000])
    ax[2].set_xlabel('Time point')
    #ax[2].set_ylabel('Aggregated\nlatent state')
    ax[2].set_ylabel('$z_{agg}$')
    ax[2].set_yticks([0, 1, 2])
    ax[2].set_yticklabels([1, 2, 3])
    ax[2].tick_params(axis='x', top=False, labelbottom=True, labeltop=False)
    ax[2].set_ylim([2.5, -0.5])
    plt.xticks(rotation=0)

    # Draw vertical dashed lines at change points
    MAP = np.argmax(q_z_lumped[1, 0, :3, 3000:5000], axis=0)
    change_points = np.where(np.diff(MAP) != 0)[0]
    for change_point in change_points:
        ax[0].axvline(change_point, c='r', ls='--', lw=.5)
        ax[1].axvline(change_point, c='r', ls='--', lw=.5)
        ax[2].axvline(change_point, c='r', ls='--', lw=.5)

    plt.tight_layout()
    plt.subplots_adjust(left=0.08, right=0.96, top=0.99, bottom=0.12, hspace=0.15)
    plt.savefig(f'../plots/3well_SDE/q_z_lumped_reconstruction_{TIMESTAMP}.pdf', format='pdf')
    plt.close()


def plot_ELBO():
    """
    Plot the ELBO trajectories of all simulations.
    """

    h5file = tables.open_file(RES_PATH + FNAME, 'a')
    ELBO = []
    for run_id, run in enumerate(h5file.root.ELBO):
        print(run_id)
        ELBO.append(np.array(run.iteration_99))
    h5file.close()

    for trajectory in ELBO:
        plt.plot(-1*trajectory[:100])

    plt.yscale('log')
    plt.show()
    pass


plot_posterior_NIW()
#plot_ELBO()
#plot_KLDR()
plot_q_z()
#plot_n_modes()

